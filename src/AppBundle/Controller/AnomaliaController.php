<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Anomalia;
use AppBundle\Entity\AnomaliaDetalle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Anomalia controller.
 *
 * @Route("anomalia")
 */
class AnomaliaController extends Controller
{
    /**
     * Lists all anomalia entities.
     *
     * @Route("/", name="anomalia_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $anomalias = $em->getRepository('AppBundle:Anomalia')->findByActivo(1);

        return $this->render('anomalia/index.html.twig', array(
            'anomalias' => $anomalias,
        ));
    }

    /**
     * Creates a new anomalia entity.
     *
     * @Route("/new", name="anomalia_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $usuario = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        // Clasificaciones: procedimientos/herramientas/epp/condicion/sistemas/comportamiento
        $procedimientos = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("procedimientos");
        $herramientas = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("herramientas");
        $epps = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("epp");
        $condiciones = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("condicion");
        $sistemas = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("sistemas");
        $comportamientos = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("comportamiento");

        $anomalia = new Anomalia();
        $form = $this->createForm('AppBundle\Form\AnomaliaType', $anomalia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // registrando la anomalia
            $anomalia->setFechaRegistro(new \DateTime('now'));
            $anomalia->setUsuario($usuario);

            // Referencia de la anomalia: LOCALIDAD+GERENCIA+AÑO+N°
            $localidad = $anomalia->getSitio()->getCodigo();
            $gerencia = $anomalia->getGerencia()->getCodigo();
            $year = date("y");
            $ultimo = $em->getRepository('AppBundle:Anomalia')->getLastIDAnomalia();
            $numero = $ultimo + 1;

            $referencia = $localidad."-".$gerencia."-".$year."-".$numero;
            $anomalia->setReferencia($referencia);
            $anomalia->setActivo(1);

            $em->persist($anomalia);
            $em->flush();

            // procedimientos
            if ($request->get('p1')){
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'p1'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('p2')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'p2'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('p3')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'p3'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('p4')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'p4'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('p5')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'p5'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('p6')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'p6'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('p7')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'p7'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('p8')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'p8'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }

            // herramientas / equipos
            if ($request->get('he1')){
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'he1'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('he2')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'he2'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('he3')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'he3'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('he4')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'he4'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('he5')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'he5'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('he6')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'he6'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('he7')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'he7'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }

            // epp
            if ($request->get('epp1')){
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'epp1'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('epp2')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'epp2'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('epp3')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'epp3'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }

            // condicion del sitio de trabajo
            if ($request->get('c1')){
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'c1'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('c2')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'c2'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('c3')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'c3'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('c4')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'c4'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('c5')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'c5'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('c6')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'c6'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('c7')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'c7'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('c8')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'c8'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('c9')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'c9'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }

            // sistemas de seguridad
            if ($request->get('s1')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 's1'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('s2')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 's2'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('s3')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 's3'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('s4')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 's4'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('s5')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 's5'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('s6')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 's6'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }

            //comportamiento
            if ($request->get('cp1')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'cp1'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('cp2')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'cp2'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('cp3')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'cp3'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('cp4')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'cp4'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('cp5')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'cp5'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }
            if ($request->get('cp6')) {
                $detalle = new AnomaliaDetalle();
                $detalle->setAnomalia($anomalia);

                $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => 'cp6'));
                $detalle->setClasificacion($clasificacion);

                $em->persist($detalle);
                $em->flush();
            }

            $message = \Swift_Message::newInstance()
                ->setSubject('Sistema de Anomalías - Notificación de registro de anomalía')
                ->setFrom('anomalias@ypergas.com')
                ->setTo(['jcelis@ypergas.com', 'jane.celis@gmail.com'])
                ->setBody('Se ha registrado su anomalia bajo la referencia N° XXX-XX.',
                    'text/html'
                )
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
            ;
            $this->get('mailer')->send($message);

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "ADD_ANM", "REF: ".$anomalia->getReferencia().", ID: ".$anomalia->getId());

            return $this->redirectToRoute('anomalia_show', array('id' => $anomalia->getId()));
        }

        return $this->render('anomalia/sha.html.twig', array(
            'procedimientos' => $procedimientos,
            'herramientas' => $herramientas,
            'epps' => $epps,
            'condiciones' => $condiciones,
            'sistemas' => $sistemas,
            'comportamientos' => $comportamientos,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a anomalia entity.
     *
     * @Route("/{id}", name="anomalia_show")
     * @Method("GET")
     */
    public function showAction(Anomalia $anomalia)
    {
        $em = $this->getDoctrine()->getManager();

        // Detalle de la anomalia
        $detalles = $em->getRepository('AppBundle:AnomaliaDetalle')->findBy(array('anomalia' => $anomalia));

        // Detalle para las clasificaciones: procedimientos/herramientas/epp/condicion/sistemas/comportamiento
        $procedimientos = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasDetallesByCategoria($anomalia, "procedimientos");
        $herramientas = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasDetallesByCategoria($anomalia, "herramientas");
        $epps = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasDetallesByCategoria($anomalia, "epp");
        $condiciones = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasDetallesByCategoria($anomalia, "condicion");
        $sistemas = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasDetallesByCategoria($anomalia, "sistemas");
        $comportamientos = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasDetallesByCategoria($anomalia, "comportamiento");

        $deleteForm = $this->createDeleteForm($anomalia);

        return $this->render('anomalia/show.html.twig', array(
            'anomalia' => $anomalia,
            'detalles' => $detalles,
            'procedimientos' => $procedimientos,
            'herramientas' => $herramientas,
            'epps' => $epps,
            'condiciones' => $condiciones,
            'sistemas' => $sistemas,
            'comportamientos' => $comportamientos,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Ver anomalia en formato de Tarjeta
     *
     * @Route("/tarjeta/{id}", name="anomalia_ver")
     * @Method("GET")
     */
    public function verAction(Anomalia $anomalia)
    {
        $em = $this->getDoctrine()->getManager();

        // Clasificaciones: procedimientos/herramientas/epp/condicion/sistemas/comportamiento
        $procedimientos = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("procedimientos");
        $herramientas = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("herramientas");
        $epps = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("epp");
        $condiciones = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("condicion");
        $sistemas = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("sistemas");
        $comportamientos = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("comportamiento");

        // Detalle de la anomalia
        $detalles = $em->getRepository('AppBundle:AnomaliaDetalle')->findBy(array('anomalia' => $anomalia));

        $deleteForm = $this->createDeleteForm($anomalia);

        return $this->render('anomalia/ver.html.twig', array(
            'anomalia' => $anomalia,
            'detalles' => $detalles,
            'procedimientos' => $procedimientos,
            'herramientas' => $herramientas,
            'epps' => $epps,
            'condiciones' => $condiciones,
            'sistemas' => $sistemas,
            'comportamientos' => $comportamientos,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing anomalia entity.
     *
     * @Route("/{id}/edit", name="anomalia_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Anomalia $anomalia)
    {
        $em = $this->getDoctrine()->getManager();

        // Detalle de la anomalia
        $detalles = $em->getRepository('AppBundle:AnomaliaDetalle')->getClasificacionesInAnomaliaDetalle($anomalia);

        // Clasificaciones: procedimientos/herramientas/epp/condicion/sistemas/comportamiento
        $procedimientos = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("procedimientos");
        $herramientas = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("herramientas");
        $epps = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("epp");
        $condiciones = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("condicion");
        $sistemas = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("sistemas");
        $comportamientos = $em->getRepository('AppBundle:Clasificacion')->getAllClasificacionesByCategoria("comportamiento");

        $deleteForm = $this->createDeleteForm($anomalia);
        $editForm = $this->createForm('AppBundle\Form\AnomaliaType', $anomalia);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $eliminarProcedimientos = null;
            $eliminarHerramientas = null;
            $eliminarEPP = null;
            $eliminarCondicion = null;
            $eliminarSistemas = null;
            $eliminarComportamiento = null;

            // *** Procedimientos *** //
            foreach ($procedimientos as $procedimiento) {
                $codigoClasif = $procedimiento->getCodigo();

                // Verificar si el procedimiento ha sido seleccionado
                // En caso contrario, se verifica si existe en detalles para su eliminacion (ya que el usuario lo deselecciono)
                if ($request->get($codigoClasif)) {
                    // Si existe no existe como detalle se agrega
                    if (!in_array($codigoClasif, $detalles)) {
                        $detalle = new AnomaliaDetalle();
                        $detalle->setAnomalia($anomalia);

                        $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $codigoClasif));
                        $detalle->setClasificacion($clasificacion);

                        $em->persist($detalle);
                        $em->flush();
                    }
                }else{
                    if(in_array($codigoClasif, $detalles)) {
                        $eliminarProcedimientos[] = $codigoClasif;
                    }
                }
            }

            if($eliminarProcedimientos) {
                // Eliminando los detalles en arreglo $eliminarProcedimientos
                foreach ($eliminarProcedimientos as $eliminar) {
                    // Consulto la clasificacion asociada al codigo en $eliminar
                    $clasificacionEliminar = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $eliminar));

                    // Consulto el detalle de anomalia a eliminar
                    $detalleEliminar = $em->getRepository('AppBundle:AnomaliaDetalle')->findOneBy(array('anomalia' => $anomalia, 'clasificacion' => $clasificacionEliminar));

                    $em->remove($detalleEliminar);
                    $em->flush();
                }
            }
            // *** Fin Procedimientos *** //


            // *** Herramientas / Equipos *** //
            foreach ($herramientas as $herramienta) {
                $codigoClasif = $herramienta->getCodigo();

                // Verificar si ela herramienta/equipo ha sido seleccionado
                // En caso contrario, se verifica si existe en detalles para su eliminacion (ya que el usuario lo deselecciono)
                if ($request->get($codigoClasif)) {
                    // Si existe no existe como detalle se agrega
                    if (!in_array($codigoClasif, $detalles)) {
                        $detalle = new AnomaliaDetalle();
                        $detalle->setAnomalia($anomalia);

                        $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $codigoClasif));
                        $detalle->setClasificacion($clasificacion);

                        $em->persist($detalle);
                        $em->flush();
                    }
                }else{
                    if(in_array($codigoClasif, $detalles)) {
                        $eliminarHerramientas[] = $codigoClasif;
                    }
                }
            }

            if ($eliminarHerramientas) {
                // Eliminando los detalles en arreglo $eliminarHerramientas
                foreach ($eliminarHerramientas as $eliminar) {
                    // Consulto la clasificacion asociada al codigo en $eliminar
                    $clasificacionEliminar = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $eliminar));

                    // Consulto el detalle de anomalia a eliminar
                    $detalleEliminar = $em->getRepository('AppBundle:AnomaliaDetalle')->findOneBy(array('anomalia' => $anomalia, 'clasificacion' => $clasificacionEliminar));

                    $em->remove($detalleEliminar);
                    $em->flush();
                }
            }
            // *** Fin Herramientas / Equipos *** //


            // *** EPP *** //
            foreach ($epps as $epp) {
                $codigoClasif = $epp->getCodigo();

                // Verificar si el epp ha sido seleccionado
                // En caso contrario, se verifica si existe en detalles para su eliminacion (ya que el usuario lo deselecciono)
                if ($request->get($codigoClasif)) {
                    // Si existe no existe como detalle se agrega
                    if (!in_array($codigoClasif, $detalles)) {
                        $detalle = new AnomaliaDetalle();
                        $detalle->setAnomalia($anomalia);

                        $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $codigoClasif));
                        $detalle->setClasificacion($clasificacion);

                        $em->persist($detalle);
                        $em->flush();
                    }
                }else{
                    if(in_array($codigoClasif, $detalles)) {
                        $eliminarEPP[] = $codigoClasif;
                    }
                }
            }

            if($eliminarEPP) {
                // Eliminando los detalles en arreglo $eliminarEPP
                foreach ($eliminarEPP as $eliminar) {
                    // Consulto la clasificacion asociada al codigo en $eliminar
                    $clasificacionEliminar = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $eliminar));

                    // Consulto el detalle de anomalia a eliminar
                    $detalleEliminar = $em->getRepository('AppBundle:AnomaliaDetalle')->findOneBy(array('anomalia' => $anomalia, 'clasificacion' => $clasificacionEliminar));

                    $em->remove($detalleEliminar);
                    $em->flush();
                }
            }
            // *** Fin EPP *** //


            // *** Condicion del Sitio de trabajo *** //
            foreach ($condiciones as $condicion) {
                $codigoClasif = $condicion->getCodigo();

                // Verificar si la condicion ha sido seleccionada
                // En caso contrario, se verifica si existe en detalles para su eliminacion (ya que el usuario lo deselecciono)
                if ($request->get($codigoClasif)) {
                    // Si existe no existe como detalle se agrega
                    if (!in_array($codigoClasif, $detalles)) {
                        $detalle = new AnomaliaDetalle();
                        $detalle->setAnomalia($anomalia);

                        $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $codigoClasif));
                        $detalle->setClasificacion($clasificacion);

                        $em->persist($detalle);
                        $em->flush();
                    }
                }else{
                    if(in_array($codigoClasif, $detalles)) {
                        $eliminarCondicion[] = $codigoClasif;
                    }
                }
            }

            if($eliminarCondicion) {
                // Eliminando los detalles en arreglo $eliminarCondicion
                foreach ($eliminarCondicion as $eliminar) {
                    // Consulto la clasificacion asociada al codigo en $eliminar
                    $clasificacionEliminar = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $eliminar));

                    // Consulto el detalle de anomalia a eliminar
                    $detalleEliminar = $em->getRepository('AppBundle:AnomaliaDetalle')->findOneBy(array('anomalia' => $anomalia, 'clasificacion' => $clasificacionEliminar));

                    $em->remove($detalleEliminar);
                    $em->flush();
                }
            }
            // *** Fin Condicion del Sitio de trabajo *** //


            // *** Sistemas de seguridad *** //
            foreach ($sistemas as $sistema) {
                $codigoClasif = $sistema->getCodigo();

                // Verificar si el sistema ha sido seleccionado
                // En caso contrario, se verifica si existe en detalles para su eliminacion (ya que el usuario lo deselecciono)
                if ($request->get($codigoClasif)) {
                    // Si existe no existe como detalle se agrega
                    if (!in_array($codigoClasif, $detalles)) {
                        $detalle = new AnomaliaDetalle();
                        $detalle->setAnomalia($anomalia);

                        $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $codigoClasif));
                        $detalle->setClasificacion($clasificacion);

                        $em->persist($detalle);
                        $em->flush();
                    }
                }else{
                    if(in_array($codigoClasif, $detalles)) {
                        $eliminarSistemas[] = $codigoClasif;
                    }
                }
            }

            if($eliminarSistemas) {
                // Eliminando los detalles en arreglo $eliminarSistemas
                foreach ($eliminarSistemas as $eliminar) {
                    // Consulto la clasificacion asociada al codigo en $eliminar
                    $clasificacionEliminar = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $eliminar));

                    // Consulto el detalle de anomalia a eliminar
                    $detalleEliminar = $em->getRepository('AppBundle:AnomaliaDetalle')->findOneBy(array('anomalia' => $anomalia, 'clasificacion' => $clasificacionEliminar));

                    $em->remove($detalleEliminar);
                    $em->flush();
                }
            }
            // *** Fin Sistemas de seguridad *** //


            // *** Comportamiento *** //
            foreach ($comportamientos as $comportamiento) {
                $codigoClasif = $comportamiento->getCodigo();

                // Verificar si el comportamiento ha sido seleccionado
                // En caso contrario, se verifica si existe en detalles para su eliminacion (ya que el usuario lo deselecciono)
                if ($request->get($codigoClasif)) {
                    // Si existe no existe como detalle se agrega
                    if (!in_array($codigoClasif, $detalles)) {
                        $detalle = new AnomaliaDetalle();
                        $detalle->setAnomalia($anomalia);

                        $clasificacion = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $codigoClasif));
                        $detalle->setClasificacion($clasificacion);

                        $em->persist($detalle);
                        $em->flush();
                    }
                }else{
                    if(in_array($codigoClasif, $detalles)) {
                        $eliminarComportamiento[] = $codigoClasif;
                    }
                }
            }

            if($eliminarComportamiento) {
                // Eliminando los detalles en arreglo $eliminarComportamiento
                foreach ($eliminarComportamiento as $eliminar) {
                    // Consulto la clasificacion asociada al codigo en $eliminar
                    $clasificacionEliminar = $em->getRepository('AppBundle:Clasificacion')->findOneBy(array('codigo' => $eliminar));

                    // Consulto el detalle de anomalia a eliminar
                    $detalleEliminar = $em->getRepository('AppBundle:AnomaliaDetalle')->findOneBy(array('anomalia' => $anomalia, 'clasificacion' => $clasificacionEliminar));

                    $em->remove($detalleEliminar);
                    $em->flush();
                }
            }
            // *** Fin Comportamiento *** //

            $this->getDoctrine()->getManager()->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "EDIT_ANM", "REF: ".$anomalia->getReferencia().", ID: ".$anomalia->getId());

            return $this->redirectToRoute('anomalia_edit', array('id' => $anomalia->getId()));
        }

        return $this->render('anomalia/editar.html.twig', array(
            'anomalia' => $anomalia,
            'detalles' => $detalles,
            'procedimientos' => $procedimientos,
            'herramientas' => $herramientas,
            'epps' => $epps,
            'condiciones' => $condiciones,
            'sistemas' => $sistemas,
            'comportamientos' => $comportamientos,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a anomalia entity.
     *
     * @Route("/{id}", name="anomalia_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Anomalia $anomalia)
    {
        $form = $this->createDeleteForm($anomalia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Eliminar logicamente la anomalia cambiando el campo 'activo' a 0
            $anomalia->setActivo(0);
            $em->persist($anomalia);
            $em->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "DELETE_ANM", "REF: ".$anomalia->getReferencia().", ID: ".$anomalia->getId());
        }

        return $this->redirectToRoute('anomalia_index');
    }

    /**
     * Creates a form to delete a anomalia entity.
     *
     * @param Anomalia $anomalia The anomalia entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Anomalia $anomalia)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomalia_delete', array('id' => $anomalia->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
