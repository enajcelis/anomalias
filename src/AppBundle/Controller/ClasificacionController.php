<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Clasificacion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Clasificacion controller.
 *
 * @Route("clasificacion")
 */
class ClasificacionController extends Controller
{
    /**
     * Lists all clasificacion entities.
     *
     * @Route("/", name="clasificacion_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clasificacions = $em->getRepository('AppBundle:Clasificacion')->findAll();

        return $this->render('clasificacion/index.html.twig', array(
            'clasificacions' => $clasificacions,
        ));
    }

    /**
     * Creates a new clasificacion entity.
     *
     * @Route("/new", name="clasificacion_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $clasificacion = new Clasificacion();
        $form = $this->createForm('AppBundle\Form\ClasificacionType', $clasificacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($clasificacion);
            $em->flush();

            return $this->redirectToRoute('clasificacion_show', array('id' => $clasificacion->getId()));
        }

        return $this->render('clasificacion/new.html.twig', array(
            'clasificacion' => $clasificacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a clasificacion entity.
     *
     * @Route("/{id}", name="clasificacion_show")
     * @Method("GET")
     */
    public function showAction(Clasificacion $clasificacion)
    {
        $deleteForm = $this->createDeleteForm($clasificacion);

        return $this->render('clasificacion/show.html.twig', array(
            'clasificacion' => $clasificacion,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing clasificacion entity.
     *
     * @Route("/{id}/edit", name="clasificacion_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Clasificacion $clasificacion)
    {
        $deleteForm = $this->createDeleteForm($clasificacion);
        $editForm = $this->createForm('AppBundle\Form\ClasificacionType', $clasificacion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clasificacion_edit', array('id' => $clasificacion->getId()));
        }

        return $this->render('clasificacion/edit.html.twig', array(
            'clasificacion' => $clasificacion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a clasificacion entity.
     *
     * @Route("/{id}", name="clasificacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Clasificacion $clasificacion)
    {
        $form = $this->createDeleteForm($clasificacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($clasificacion);
            $em->flush();
        }

        return $this->redirectToRoute('clasificacion_index');
    }

    /**
     * Creates a form to delete a clasificacion entity.
     *
     * @param Clasificacion $clasificacion The clasificacion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Clasificacion $clasificacion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clasificacion_delete', array('id' => $clasificacion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
