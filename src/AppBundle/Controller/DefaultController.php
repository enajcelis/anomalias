<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Estatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * DefaultController.
 */
class DefaultController extends Controller
{

    /**
     * @Route("/", name="app_homepage")
     *
     * @param Request $request Request
     *
     * @return Response
     */
    public function homepageAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $total = $em->getRepository('AppBundle:Anomalia')->getTotalAnomalias();

        $abiertas = $em->getRepository('AppBundle:Anomalia')->getTotalAnomaliasByEstatus(Estatus::ID_ABIERTA);
        $cerradas = $em->getRepository('AppBundle:Anomalia')->getTotalAnomaliasByEstatus(Estatus::ID_CERRADA);
        $pct_abiertas = round(($abiertas * 100) / $total, 2);
        $pct_cerradas = round(($cerradas * 100) / $total, 2);

        $hoy = $em->getRepository('AppBundle:Anomalia')->getTotalAnomaliasByFecha("now");

        $caracas = $em->getRepository('AppBundle:Anomalia')->findBy(array('sitio' => 1, 'activo' => 1));
        $altagracia = $em->getRepository('AppBundle:Anomalia')->findBy(array('sitio' => 2, 'activo' => 1));
        $base = $em->getRepository('AppBundle:Anomalia')->findBy(array('sitio' => 3, 'activo' => 1));
        $ccs = count($caracas);
        $aor = count($altagracia);
        $bao = count($base);
        $pct_caracas = round(($ccs * 100) / $total, 2);
        $pct_altagracia = round(($aor * 100) / $total, 2);
        $pct_base = round(($bao * 100) / $total, 2);

        $anm_vencidas = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByVencimiento("vencidas");
        $vencidas = count($anm_vencidas);

        $anm_novencidas = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByVencimiento("novencidas");
        $no_vencidas = count($anm_novencidas);

        return $this->render('app/pages/dashboard.html.twig', array(
            'total' => $total,
            'abiertas' => $abiertas,
            'pct_abiertas' => $pct_abiertas,
            'cerradas' => $cerradas,
            'pct_cerradas' => $pct_cerradas,
            'hoy' => $hoy,
            'caracas' => $ccs,
            'altagracia' => $aor,
            'bao' => $bao,
            'pct_caracas' => $pct_caracas,
            'pct_altagracia' => $pct_altagracia,
            'pct_base' => $pct_base,
            'vencidas' => $vencidas,
            'no_vencidas' => $no_vencidas,
        ) );
    }
}
