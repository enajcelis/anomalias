<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Estatus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Estatus controller.
 *
 * @Route("estatus")
 */
class EstatusController extends Controller
{
    /**
     * Lists all estatus entities.
     *
     * @Route("/", name="estatus_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $estatuses = $em->getRepository('AppBundle:Estatus')->findAll();

        return $this->render('estatus/index.html.twig', array(
            'estatuses' => $estatuses,
        ));
    }

    /**
     * Creates a new estatus entity.
     *
     * @Route("/new", name="estatus_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $estatus = new Estatus();
        $form = $this->createForm('AppBundle\Form\EstatusType', $estatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($estatus);
            $em->flush();

            return $this->redirectToRoute('estatus_show', array('id' => $estatus->getId()));
        }

        return $this->render('estatus/new.html.twig', array(
            'estatus' => $estatus,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a estatus entity.
     *
     * @Route("/{id}", name="estatus_show")
     * @Method("GET")
     */
    public function showAction(Estatus $estatus)
    {
        $deleteForm = $this->createDeleteForm($estatus);

        return $this->render('estatus/show.html.twig', array(
            'estatus' => $estatus,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing estatus entity.
     *
     * @Route("/{id}/edit", name="estatus_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Estatus $estatus)
    {
        $deleteForm = $this->createDeleteForm($estatus);
        $editForm = $this->createForm('AppBundle\Form\EstatusType', $estatus);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('estatus_edit', array('id' => $estatus->getId()));
        }

        return $this->render('estatus/edit.html.twig', array(
            'estatus' => $estatus,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a estatus entity.
     *
     * @Route("/{id}", name="estatus_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Estatus $estatus)
    {
        $form = $this->createDeleteForm($estatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($estatus);
            $em->flush();
        }

        return $this->redirectToRoute('estatus_index');
    }

    /**
     * Creates a form to delete a estatus entity.
     *
     * @param Estatus $estatus The estatus entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Estatus $estatus)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('estatus_delete', array('id' => $estatus->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
