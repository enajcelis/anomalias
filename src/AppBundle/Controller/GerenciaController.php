<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Gerencia;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * gerencia controller.
 *
 * @Route("gerencia")
 */
class GerenciaController extends Controller
{
    /**
     * Lists all gerencia entities.
     *
     * @Route("/", name="gerencia_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $gerencias = $em->getRepository('AppBundle:Gerencia')->findAll();

        return $this->render('gerencia/index.html.twig', array(
            'gerencias' => $gerencias,
        ));
    }

    /**
     * Creates a new gerencia entity.
     *
     * @Route("/new", name="gerencia_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $gerencia = new Gerencia();
        $form = $this->createForm('AppBundle\Form\GerenciaType', $gerencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($gerencia);
            $em->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "ADD_GERENCIA", "ID: ".$gerencia->getId().", COD: ".$gerencia->getCodigo().", NOMBRE: ".$gerencia->getNombre());

            return $this->redirectToRoute('gerencia_show', array('id' => $gerencia->getId()));
        }

        return $this->render('gerencia/new.html.twig', array(
            'gerencia' => $gerencia,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a gerencia entity.
     *
     * @Route("/{id}", name="gerencia_show")
     * @Method("GET")
     */
    public function showAction(Gerencia $gerencia)
    {
        $deleteForm = $this->createDeleteForm($gerencia);

        return $this->render('gerencia/show.html.twig', array(
            'gerencia' => $gerencia,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing gerencia entity.
     *
     * @Route("/{id}/edit", name="gerencia_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Gerencia $gerencia)
    {
        $deleteForm = $this->createDeleteForm($gerencia);
        $editForm = $this->createForm('AppBundle\Form\GerenciaType', $gerencia);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "EDIT_GERENCIA", "ID: ".$gerencia->getId().", COD: ".$gerencia->getCodigo().", NOMBRE: ".$gerencia->getNombre());

            return $this->redirectToRoute('gerencia_edit', array('id' => $gerencia->getId()));
        }

        return $this->render('gerencia/edit.html.twig', array(
            'gerencia' => $gerencia,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a gerencia entity.
     *
     * @Route("/{id}", name="gerencia_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Gerencia $gerencia)
    {
        $form = $this->createDeleteForm($gerencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($gerencia);
            $em->flush();
        }

        return $this->redirectToRoute('gerencia_index');
    }

    /**
     * Creates a form to delete a gerencia entity.
     *
     * @param Gerencia $gerencia The gerencia entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Gerencia $gerencia)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gerencia_delete', array('id' => $gerencia->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
