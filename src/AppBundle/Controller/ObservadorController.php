<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Observador;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Observador controller.
 *
 * @Route("observador")
 */
class ObservadorController extends Controller
{
    /**
     * Lists all observador entities.
     *
     * @Route("/", name="observador_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $observadors = $em->getRepository('AppBundle:Observador')->findAll();

        return $this->render('observador/index.html.twig', array(
            'observadors' => $observadors,
        ));
    }

    /**
     * Creates a new observador entity.
     *
     * @Route("/new", name="observador_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $observador = new Observador();
        $form = $this->createForm('AppBundle\Form\ObservadorType', $observador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($observador);
            $em->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "ADD_OBSERVADOR", "ID: ".$observador->getId().", NOMBRE: ".$observador->getNombre());

            return $this->redirectToRoute('observador_show', array('id' => $observador->getId()));
        }

        return $this->render('observador/new.html.twig', array(
            'observador' => $observador,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a observador entity.
     *
     * @Route("/{id}", name="observador_show")
     * @Method("GET")
     */
    public function showAction(Observador $observador)
    {
        $deleteForm = $this->createDeleteForm($observador);

        return $this->render('observador/show.html.twig', array(
            'observador' => $observador,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing observador entity.
     *
     * @Route("/{id}/edit", name="observador_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Observador $observador)
    {
        $deleteForm = $this->createDeleteForm($observador);
        $editForm = $this->createForm('AppBundle\Form\ObservadorType', $observador);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "EDIT_OBSERVADOR", "ID: ".$observador->getId().", NOMBRE: ".$observador->getNombre());

            return $this->redirectToRoute('observador_edit', array('id' => $observador->getId()));
        }

        return $this->render('observador/edit.html.twig', array(
            'observador' => $observador,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a observador entity.
     *
     * @Route("/{id}", name="observador_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Observador $observador)
    {
        $form = $this->createDeleteForm($observador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($observador);
            $em->flush();
        }

        return $this->redirectToRoute('observador_index');
    }

    /**
     * Creates a form to delete a observador entity.
     *
     * @param Observador $observador The observador entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Observador $observador)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('observador_delete', array('id' => $observador->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
