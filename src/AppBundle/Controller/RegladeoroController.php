<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Regladeoro;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Regladeoro controller.
 *
 * @Route("regladeoro")
 */
class RegladeoroController extends Controller
{
    /**
     * Lists all regladeoro entities.
     *
     * @Route("/", name="regladeoro_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $regladeoros = $em->getRepository('AppBundle:Regladeoro')->findAll();

        return $this->render('regladeoro/index.html.twig', array(
            'regladeoros' => $regladeoros,
        ));
    }

    /**
     * Creates a new regladeoro entity.
     *
     * @Route("/new", name="regladeoro_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $regladeoro = new Regladeoro();
        $form = $this->createForm('AppBundle\Form\RegladeoroType', $regladeoro);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($regladeoro);
            $em->flush();

            return $this->redirectToRoute('regladeoro_show', array('id' => $regladeoro->getId()));
        }

        return $this->render('regladeoro/new.html.twig', array(
            'regladeoro' => $regladeoro,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a regladeoro entity.
     *
     * @Route("/{id}", name="regladeoro_show")
     * @Method("GET")
     */
    public function showAction(Regladeoro $regladeoro)
    {
        $deleteForm = $this->createDeleteForm($regladeoro);

        return $this->render('regladeoro/show.html.twig', array(
            'regladeoro' => $regladeoro,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing regladeoro entity.
     *
     * @Route("/{id}/edit", name="regladeoro_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Regladeoro $regladeoro)
    {
        $deleteForm = $this->createDeleteForm($regladeoro);
        $editForm = $this->createForm('AppBundle\Form\RegladeoroType', $regladeoro);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('regladeoro_edit', array('id' => $regladeoro->getId()));
        }

        return $this->render('regladeoro/edit.html.twig', array(
            'regladeoro' => $regladeoro,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a regladeoro entity.
     *
     * @Route("/{id}", name="regladeoro_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Regladeoro $regladeoro)
    {
        $form = $this->createDeleteForm($regladeoro);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($regladeoro);
            $em->flush();
        }

        return $this->redirectToRoute('regladeoro_index');
    }

    /**
     * Creates a form to delete a regladeoro entity.
     *
     * @param Regladeoro $regladeoro The regladeoro entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Regladeoro $regladeoro)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('regladeoro_delete', array('id' => $regladeoro->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
