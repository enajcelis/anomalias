<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Observador;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * ReportesController.
 *
 * @Route("reportes")
 */
class ReportesController extends Controller
{

    /**
     * Pantalla solicitud de parametros
     * Reporte de anomalias por Observador
     *
     * @Route("/1", name="pantalla_porobservador")
     * @Method("GET")
     */
    public function pantallaReportePorObservadorAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $observadores = $em->getRepository('AppBundle:Observador')->findAllOrderedByName();

        return $this->render('reportes/pantalla_por_observador.html.twig', array(
            'observadores' => $observadores,
        ) );
    }

    /**
     * Reporte de anomalias por Observador
     *
     * @Route("/1/do", name="reporte_porobservador")
     * @Method("POST")
     */
    public function reportePorObservadorAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idObservador = $request->get('observador');

        $observador = $em->getRepository('AppBundle:Observador')->findOneBy(array('id' => $idObservador));
        $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByObservador($observador);
        $cantidad = count($anomalias);

        return $this->render('reportes/reporte_por_observador.html.twig', array(
            'observador' => $observador,
            'anomalias' => $anomalias,
            'idObservador' => $idObservador,
            'cantidad' => $cantidad,
        ) );
    }

    /**
     * Pantalla solicitud de parametros
     * Reporte de anomalias por Ubicación / Sitio
     *
     * @Route("/2", name="pantalla_porsitio")
     * @Method("GET")
     */
    public function pantallaReportePorSitioAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sitios = $em->getRepository('AppBundle:Sitio')->findAllOrderedByName();

        return $this->render('reportes/pantalla_por_sitio.html.twig', array(
            'sitios' => $sitios,
        ) );
    }

    /**
     * Reporte de anomalias por Sitio / Ubicación
     *
     * @Route("/2/do", name="reporte_porsitio")
     * @Method("POST")
     */
    public function reportePorSitioAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idSitio = $request->get('sitio');

        $sitio = $em->getRepository('AppBundle:Sitio')->findOneBy(array('id' => $idSitio));
        $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasBySitio($sitio);
        $cantidad = count($anomalias);

        return $this->render('reportes/reporte_por_sitio.html.twig', array(
            'sitio' => $sitio,
            'anomalias' => $anomalias,
            'idSitio' => $idSitio,
            'cantidad' => $cantidad,
        ) );
    }

    public function returnPDFResponseFromHTML($html, $filename){
        $pdf = $this->get("white_october.tcpdf")->create(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('YPergas');
        $pdf->SetTitle(('Sistema de Anomalías'));
        $pdf->SetSubject('YPergas');
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->SetHeaderData(false);
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        $pdf->setFooterFont(Array('helvetica', '', 8));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(10, 5, 10);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->AddPage('L', 'A4');

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = false);

        $pdf->Output($filename.".pdf",'I'); // I para abrir pdf en navegador, D para descargar pdf
    }

    /**
     * Genera reporte en formato PDF
     *
     * @Route("/{id}/pdf", name="pdf")
     * @Method({"GET", "POST"})
     */
    public function generarPDFAction(Request $request)
    {
        $generado = date("dmY");

        // Reporte por Observador (1)
        if( $request->request->get('reporte') == 1 ){
            $idObservador = $request->get('observador');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $observador = $em->getRepository('AppBundle:Observador')->findOneBy(array('id' => $idObservador));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByObservador($observador);
            $cantidad = count($anomalias);

            $filename = 'ANM_observador_'.$idObservador.'_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_por_observador_pdf.html.twig',
                array(
                    'observador' => $observador,
                    'anomalias' => $anomalias,
                    'idObservador' => $idObservador,
                    'cantidad' => $cantidad,
                )
            );
        }

        // Reporte por Ubicación / Sitio (2)
        if ( $request->request->get('reporte') == 2 ){
            $idSitio = $request->get('sitio');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $sitio = $em->getRepository('AppBundle:Sitio')->findOneBy(array('id' => $idSitio));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasBySitio($sitio);
            $cantidad = count($anomalias);

            $filename = 'ANM_sitio_'.$sitio->getCodigo().'_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_por_sitio_pdf.html.twig',
                array(
                    'sitio' => $sitio,
                    'anomalias' => $anomalias,
                    'idSitio' => $idSitio,
                    'cantidad' => $cantidad,
                )
            );
        }

        // Reporte: Listado por Personas (3)
        if ( $request->request->get('reporte') == 3 ){
            $desde = $request->get('desde');
            $hasta = $request->get('hasta');
            $em = $this->getDoctrine()->getManager();

            $idsObservadores = null;
            $totalAnomalias = null;
            $observadores = null;

            // Data para el reporte
            $anm = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByFechas($desde, $hasta);
            $cantidad = count($anm);
            $idsObservadores = $em->getRepository('AppBundle:Anomalia')->getIdObservadoresByFechas($desde, $hasta);

            foreach($idsObservadores as $idObservador){
                $totalAnomalias[] = $em->getRepository('AppBundle:Anomalia')->getTotalAnomaliasByObservadorByFechas($idObservador, $desde, $hasta);
                $observador = $em->getRepository('AppBundle:Observador')->findOneBy(array('id' => $idObservador));
                $observadores[] = $observador->getNombre();
            }

            $filename = 'ANM_personas_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_por_personas_pdf.html.twig',
                array(
                    'desde' => $desde,
                    'hasta' => $hasta,
                    'observadores' => $observadores,
                    'totales' => $totalAnomalias,
                    'cantidad' => $cantidad,
                )
            );
        }

        // Reporte Pendientes (según vencimiento) (4)
        if( $request->request->get('reporte') == 4 ){
            $filtro = $request->get('filtro');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByVencimiento($filtro);
            $cantidad = count($anomalias);

            $filename = 'ANM_pendientes_'.$filtro.'_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_pendientes_pdf.html.twig',
                array(
                    'filtro' => $filtro,
                    'anomalias' => $anomalias,
                    'cantidad' => $cantidad,
                )
            );
        }

        // Reporte Anomalias Abiertas Asignadas (5)
        if ( $request->request->get('reporte') == 5 ){
            $desde = $request->get('desde');
            $hasta = $request->get('hasta');
            $sitio = $request->get('sitio');

            $em = $this->getDoctrine()->getManager();

            $idsGerencias = null;
            $totalAnomalias = null;
            $gerencias = null;

            // Consultando el nombre del sitio
            $s = $em->getRepository('AppBundle:Sitio')->findOneBy(array('id' => $sitio));
            $nombre_sitio = $s->getNombre();

            // Data para el reporte
            $anm = $em->getRepository('AppBundle:Anomalia')->getAnomaliasAbiertasSitioByFechas($desde, $hasta, $sitio);
            $cantidad = count($anm);

            if($cantidad > 0) {
                $idsGerencias = $em->getRepository('AppBundle:Anomalia')->getIdGerenciasSitioByFechas($desde, $hasta, $sitio);

                if ($idsGerencias) {
                    foreach ($idsGerencias as $idGerencia) {
                        // Consulto el total de anomalias abiertas registradas en le fecha (desde,hasta) y sitio/ubicación para la gerencia
                        $totalAnomalias[] = $em->getRepository('AppBundle:Anomalia')->getTotalAnomaliasAbiertasSitioByGerenciaByFechas($idGerencia, $desde, $hasta, $sitio);
                        $gerencia = $em->getRepository('AppBundle:Gerencia')->findOneBy(array('id' => $idGerencia));
                        $gerencias[] = $gerencia->getNombre();
                    }
                }
            }

            $filename = 'ANM_abiertas_asignadas_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_abiertas_asignadas_pdf.html.twig',
                array(
                    'desde' => $desde,
                    'hasta' => $hasta,
                    'sitio' => $sitio,
                    'nombre_sitio' => $nombre_sitio,
                    'gerencias' => $gerencias,
                    'totales' => $totalAnomalias,
                    'cantidad' => $cantidad,
                )
            );
        }

        // Reporte por Regla de Oro (6)
        if( $request->request->get('reporte') == 6 ){
            $idRegladeoro = $request->get('regladeoro');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $regladeoro = $em->getRepository('AppBundle:Regladeoro')->findOneBy(array('id' => $idRegladeoro));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByRegladeoro($regladeoro);
            $cantidad = count($anomalias);

            $filename = 'ANM_regladeoro_'.$regladeoro->getCodigo().'_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_por_regladeoro_pdf.html.twig',
                array(
                    'regladeoro' => $regladeoro,
                    'anomalias' => $anomalias,
                    'idRegladeoro' => $idRegladeoro,
                    'cantidad' => $cantidad,
                )
            );
        }

        // Reporte por Gerencia (7)
        if( $request->request->get('reporte') == 7 ){
            $idGerencia = $request->get('gerencia');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $gerencia = $em->getRepository('AppBundle:Gerencia')->findOneBy(array('id' => $idGerencia));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByGerencia($gerencia);
            $cantidad = count($anomalias);

            $filename = 'ANM_gerencia_'.$gerencia->getCodigo().'_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_por_gerencia_pdf.html.twig',
                array(
                    'gerencia' => $gerencia,
                    'anomalias' => $anomalias,
                    'idGerencia' => $idGerencia,
                    'cantidad' => $cantidad,
                )
            );
        }

        // Reporte por Usuario (8)
        if( $request->request->get('reporte') == 8 ){
            $idUsuario = $request->get('usuario');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('id' => $idUsuario));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByUsuario($usuario);
            $cantidad = count($anomalias);

            $filename = 'ANM_usuario_'.$usuario->getUsername().'_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_por_usuario_pdf.html.twig',
                array(
                    'usuario' => $usuario,
                    'anomalias' => $anomalias,
                    'idUsuario' => $idUsuario,
                    'cantidad' => $cantidad,
                )
            );
        }

        // Reporte por Tipo de Clasificación (9)
        if( $request->request->get('reporte') == 9 ){
            $desde = $request->get('desde');
            $hasta = $request->get('hasta');
            $tipo = $request->get('tipo');

            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $anomalias = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasPorTipoByFechas($desde, $hasta, $tipo);
            $cantidad = count($anomalias);

            $filename = 'ANM_tipo_'.$tipo.'_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_por_tipo_pdf.html.twig',
                array(
                    'desde' => $desde,
                    'hasta' => $hasta,
                    'tipo' => $tipo,
                    'anomalias' => $anomalias,
                    'cantidad' => $cantidad,
                )
            );
        }

        // Reporte por Clasificación (10)
        if( $request->request->get('reporte') == 10 ){
            $desde = $request->get('desde');
            $hasta = $request->get('hasta');
            $clasificacion = $request->get('clasificacion');

            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $anomalias = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasPorClasificacionByFechas($desde, $hasta, $clasificacion);
            $cantidad = count($anomalias);

            $filename = 'ANM_clasificacion_'.$clasificacion.'_'.$generado;

            $html = $this->renderView(
                'reportes/reporte_por_clasificacion_pdf.html.twig',
                array(
                    'desde' => $desde,
                    'hasta' => $hasta,
                    'clasificacion' => $clasificacion,
                    'anomalias' => $anomalias,
                    'cantidad' => $cantidad,
                )
            );
        }

        $this->returnPDFResponseFromHTML($html, $filename);
    }

    /**
     * Genera reporte en formato Excel
     *
     * @Route("/{id}/excel", name="excel")
     * @Method({"GET", "POST"})
     */
    public function generarExcelAction(Request $request)
    {
        $generado = date("dmY");

        // Reporte por Observador (1)
        if( $request->request->get('reporte') == 1 ){
            $idObservador = $request->get('observador');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $observador = $em->getRepository('AppBundle:Observador')->findOneBy(array('id' => $idObservador));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByObservador($observador);
            $cantidad = count($anomalias);

            if($cantidad > 0) {
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Referencia')
                    ->setCellValue('B1', 'Fecha de observación')
                    ->setCellValue('C1', 'Anomalía reportada')
                    ->setCellValue('D1', 'Acciones correctivas')
                    ->setCellValue('E1', 'Sitio')
                    ->setCellValue('F1', 'Gerencia asignada')
                    ->setCellValue('G1', 'Estatus')
                    ->setCellValue('H1', 'Fecha propuesta');

                $i = 2;
                foreach ($anomalias as $anomalia) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $anomalia->getReferencia())
                        ->setCellValue('B' . $i, date_format($anomalia->getFechaObservacion(), "d-m-Y"))
                        ->setCellValue('C' . $i, $anomalia->getAnomalia())
                        ->setCellValue('D' . $i, $anomalia->getAccionesCorrectivas())
                        ->setCellValue('E' . $i, $anomalia->getSitio()->getNombre())
                        ->setCellValue('F' . $i, $anomalia->getGerencia()->getNombre())
                        ->setCellValue('G' . $i, $anomalia->getEstatus()->getNombre())
                        ->setCellValue('H' . $i, date_format($anomalia->getFechaPropuesta(), "d-m-Y"));
                    $i++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Anomalias');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_observador_' . $idObservador . '_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

        // Reporte por Sitio / Ubicación (2)
        if ( $request->request->get('reporte') == 2 ) {
            $idSitio = $request->get('sitio');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $sitio = $em->getRepository('AppBundle:Sitio')->findOneBy(array('id' => $idSitio));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasBySitio($sitio);
            $cantidad = count($anomalias);

            if($cantidad > 0){
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Referencia')
                    ->setCellValue('B1', 'Fecha de observación')
                    ->setCellValue('C1', 'Anomalía reportada')
                    ->setCellValue('D1', 'Acciones correctivas')
                    ->setCellValue('E1', 'Gerencia asignada')
                    ->setCellValue('F1', 'Estatus')
                    ->setCellValue('G1', 'Fecha propuesta');

                $i = 2;
                foreach ($anomalias as $anomalia) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $anomalia->getReferencia())
                        ->setCellValue('B' . $i, date_format($anomalia->getFechaObservacion(), "d-m-Y"))
                        ->setCellValue('C' . $i, $anomalia->getAnomalia())
                        ->setCellValue('D' . $i, $anomalia->getAccionesCorrectivas())
                        ->setCellValue('E' . $i, $anomalia->getGerencia()->getNombre())
                        ->setCellValue('F' . $i, $anomalia->getEstatus()->getNombre())
                        ->setCellValue('G' . $i, date_format($anomalia->getFechaPropuesta(), "d-m-Y"));
                    $i++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Anomalias');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_sitio_' . $sitio->getCodigo() . '_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

        // Reporte: Listado por Personas (3)
        if ( $request->request->get('reporte') == 3 ) {
            $desde = $request->get('desde');
            $hasta = $request->get('hasta');
            $em = $this->getDoctrine()->getManager();

            $idsObservadores = null;
            $totalAnomalias = null;
            $observadores = null;

            // Data para el reporte
            $anm = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByFechas($desde, $hasta);
            $cantidad = count($anm);
            $idsObservadores = $em->getRepository('AppBundle:Anomalia')->getIdObservadoresByFechas($desde, $hasta);

            foreach($idsObservadores as $idObservador){
                $totalAnomalias[] = $em->getRepository('AppBundle:Anomalia')->getTotalAnomaliasByObservadorByFechas($idObservador, $desde, $hasta);
                $observador = $em->getRepository('AppBundle:Observador')->findOneBy(array('id' => $idObservador));
                $observadores[] = $observador->getNombre();
            }

            if($cantidad > 0){
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $desde = date("d-m-Y", strtotime($desde));
                $hasta = date("d-m-Y", strtotime($hasta));

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Desde')
                    ->setCellValue('B1', $desde)
                    ->setCellValue('A2', 'Hasta')
                    ->setCellValue('B2', $hasta)
                    ->setCellValue('A3', 'Total')
                    ->setCellValue('B3', $cantidad)
                    ->setCellValue('A5', 'Observador')
                    ->setCellValue('B5', 'Anomalías reportadas');

                $i = 6;
                $index = 0;
                foreach ($observadores as $observador) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $observador)
                        ->setCellValue('B' . $i, $totalAnomalias[$index]);
                    $i++;
                    $index++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Listado por personas');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_personas_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

        // Reporte Anomalías Pendientes (según vencimiento) (4)
        if( $request->request->get('reporte') == 4 ){
            $filtro = $request->get('filtro');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByVencimiento($filtro);
            $cantidad = count($anomalias);

            if($cantidad > 0) {
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Referencia')
                    ->setCellValue('B1', 'Fecha de observación')
                    ->setCellValue('C1', 'Anomalía reportada')
                    ->setCellValue('D1', 'Acciones correctivas')
                    ->setCellValue('E1', 'Sitio')
                    ->setCellValue('F1', 'Gerencia asignada')
                    ->setCellValue('G1', 'Fecha propuesta');

                $i = 2;
                foreach ($anomalias as $anomalia) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $anomalia->getReferencia())
                        ->setCellValue('B' . $i, date_format($anomalia->getFechaObservacion(), "d-m-Y"))
                        ->setCellValue('C' . $i, $anomalia->getAnomalia())
                        ->setCellValue('D' . $i, $anomalia->getAccionesCorrectivas())
                        ->setCellValue('E' . $i, $anomalia->getSitio()->getNombre())
                        ->setCellValue('F' . $i, $anomalia->getGerencia()->getNombre())
                        ->setCellValue('G' . $i, date_format($anomalia->getFechaPropuesta(), "d-m-Y"));
                    $i++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Anomalias');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_pendientes_' . $filtro . '_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

        // Anomalías Abiertas Asignadas (5)
        if ( $request->request->get('reporte') == 5 ) {

            $desde = $request->get('desde');
            $hasta = $request->get('hasta');
            $sitio = $request->get('sitio');
            $em = $this->getDoctrine()->getManager();

            $idsGerencias = null;
            $totalAnomalias = null;
            $gerencias = null;

            // Consultando el nombre del sitio
            $s = $em->getRepository('AppBundle:Sitio')->findOneBy(array('id' => $sitio));
            $nombre_sitio = $s->getNombre();

            // Data para el reporte
            $anm = $em->getRepository('AppBundle:Anomalia')->getAnomaliasAbiertasSitioByFechas($desde, $hasta, $sitio);
            $cantidad = count($anm);
            $idsGerencias = $em->getRepository('AppBundle:Anomalia')->getIdGerenciasSitioByFechas($desde, $hasta, $sitio);

            foreach ($idsGerencias as $idGerencia) {
                // Consulto el total de anomalias abiertas registradas en le fecha (desde,hasta) y sitio/ubicación para la gerencia
                $totalAnomalias[] = $em->getRepository('AppBundle:Anomalia')->getTotalAnomaliasAbiertasSitioByGerenciaByFechas($idGerencia, $desde, $hasta, $sitio);
                $gerencia = $em->getRepository('AppBundle:Gerencia')->findOneBy(array('id' => $idGerencia));
                $gerencias[] = $gerencia->getNombre();
            }

            if($cantidad > 0){
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $desde = date("d-m-Y", strtotime($desde));
                $hasta = date("d-m-Y", strtotime($hasta));

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Desde')
                    ->setCellValue('B1', $desde)
                    ->setCellValue('A2', 'Hasta')
                    ->setCellValue('B2', $hasta)
                    ->setCellValue('A3', 'Sitio / Ubicación')
                    ->setCellValue('B3', $nombre_sitio)
                    ->setCellValue('A4', 'Total')
                    ->setCellValue('B4', $cantidad)
                    ->setCellValue('A6', 'Gerencia asignada')
                    ->setCellValue('B6', 'Anomalías reportadas');

                $i = 7;
                $index = 0;
                foreach ($gerencias as $gerencia) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $gerencia)
                        ->setCellValue('B' . $i, $totalAnomalias[$index]);
                    $i++;
                    $index++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Anomalías');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_abiertas_asignadas_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

        // Reporte por Regla de Oro (6)
        if( $request->request->get('reporte') == 6 ){
            $idRegladeoro = $request->get('regladeoro');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $regladeoro = $em->getRepository('AppBundle:Regladeoro')->findOneBy(array('id' => $idRegladeoro));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByRegladeoro($regladeoro);
            $cantidad = count($anomalias);

            if($cantidad > 0) {
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Referencia')
                    ->setCellValue('B1', 'Fecha de observación')
                    ->setCellValue('C1', 'Anomalía reportada')
                    ->setCellValue('D1', 'Acciones correctivas')
                    ->setCellValue('E1', 'Sitio')
                    ->setCellValue('F1', 'Gerencia asignada')
                    ->setCellValue('G1', 'Estatus')
                    ->setCellValue('H1', 'Fecha propuesta');

                $i = 2;
                foreach ($anomalias as $anomalia) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $anomalia->getReferencia())
                        ->setCellValue('B' . $i, date_format($anomalia->getFechaObservacion(), "d-m-Y"))
                        ->setCellValue('C' . $i, $anomalia->getAnomalia())
                        ->setCellValue('D' . $i, $anomalia->getAccionesCorrectivas())
                        ->setCellValue('E' . $i, $anomalia->getSitio()->getNombre())
                        ->setCellValue('F' . $i, $anomalia->getGerencia()->getNombre())
                        ->setCellValue('G' . $i, $anomalia->getEstatus()->getNombre())
                        ->setCellValue('H' . $i, date_format($anomalia->getFechaPropuesta(), "d-m-Y"));
                    $i++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Anomalias');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_regladeoro_' . $regladeoro->getCodigo() . '_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

        // Reporte por Gerencia (7)
        if( $request->request->get('reporte') == 7 ){
            $idGerencia = $request->get('gerencia');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $gerencia = $em->getRepository('AppBundle:Gerencia')->findOneBy(array('id' => $idGerencia));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByGerencia($gerencia);
            $cantidad = count($anomalias);

            if($cantidad > 0) {
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Referencia')
                    ->setCellValue('B1', 'Fecha de observación')
                    ->setCellValue('C1', 'Anomalía reportada')
                    ->setCellValue('D1', 'Acciones correctivas')
                    ->setCellValue('E1', 'Observador')
                    ->setCellValue('F1', 'Sitio')
                    ->setCellValue('G1', 'Gerencia asignada')
                    ->setCellValue('H1', 'Estatus')
                    ->setCellValue('I1', 'Fecha propuesta');

                $i = 2;
                foreach ($anomalias as $anomalia) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $anomalia->getReferencia())
                        ->setCellValue('B' . $i, date_format($anomalia->getFechaObservacion(), "d-m-Y"))
                        ->setCellValue('C' . $i, $anomalia->getAnomalia())
                        ->setCellValue('D' . $i, $anomalia->getAccionesCorrectivas())
                        ->setCellValue('E' . $i, $anomalia->getObservador()->getNombre())
                        ->setCellValue('F' . $i, $anomalia->getSitio()->getNombre())
                        ->setCellValue('G' . $i, $anomalia->getGerencia()->getNombre())
                        ->setCellValue('H' . $i, $anomalia->getEstatus()->getNombre())
                        ->setCellValue('I' . $i, date_format($anomalia->getFechaPropuesta(), "d-m-Y"));
                    $i++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Anomalias');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_gerencia_' . $gerencia->getCodigo() . '_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

        // Reporte por Usuario (8)
        if( $request->request->get('reporte') == 8 ){
            $idUsuario = $request->get('usuario');
            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('id' => $idUsuario));
            $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByUsuario($usuario);
            $cantidad = count($anomalias);

            if($cantidad > 0) {
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Referencia')
                    ->setCellValue('B1', 'Fecha de observación')
                    ->setCellValue('C1', 'Anomalía reportada')
                    ->setCellValue('D1', 'Acciones correctivas')
                    ->setCellValue('E1', 'Sitio')
                    ->setCellValue('F1', 'Gerencia asignada')
                    ->setCellValue('G1', 'Estatus')
                    ->setCellValue('H1', 'Fecha propuesta');

                $i = 2;
                foreach ($anomalias as $anomalia) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $anomalia->getReferencia())
                        ->setCellValue('B' . $i, date_format($anomalia->getFechaObservacion(), "d-m-Y"))
                        ->setCellValue('C' . $i, $anomalia->getAnomalia())
                        ->setCellValue('D' . $i, $anomalia->getAccionesCorrectivas())
                        ->setCellValue('E' . $i, $anomalia->getSitio()->getNombre())
                        ->setCellValue('F' . $i, $anomalia->getGerencia()->getNombre())
                        ->setCellValue('G' . $i, $anomalia->getEstatus()->getNombre())
                        ->setCellValue('H' . $i, date_format($anomalia->getFechaPropuesta(), "d-m-Y"));
                    $i++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Anomalias');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_usuario_' . $usuario->getUsername() . '_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

        // Reporte por Tipo de Clasificación (9)
        if( $request->request->get('reporte') == 9 ){
            $desde = $request->get('desde');
            $hasta = $request->get('hasta');
            $tipo = $request->get('tipo');

            if($tipo == "acto") {
                $tipoc = "Acto inseguro";
            }else{
                $tipoc = "Condición insegura";
            }

            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $anomalias = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasPorTipoByFechas($desde, $hasta, $tipo);
            $cantidad = count($anomalias);

            if($cantidad > 0) {
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $desde = date("d-m-Y", strtotime($desde));
                $hasta = date("d-m-Y", strtotime($hasta));

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Desde')
                    ->setCellValue('B1', $desde)
                    ->setCellValue('A2', 'Hasta')
                    ->setCellValue('B2', $hasta)
                    ->setCellValue('A3', 'Tipo de Clasificación')
                    ->setCellValue('B3', $tipoc)
                    ->setCellValue('A4', 'Total')
                    ->setCellValue('B4', $cantidad)
                    ->setCellValue('A6', 'Referencia')
                    ->setCellValue('B6', 'Fecha de observación')
                    ->setCellValue('C6', 'Anomalía reportada')
                    ->setCellValue('D6', 'Acciones correctivas')
                    ->setCellValue('E6', 'Sitio')
                    ->setCellValue('F6', 'Gerencia asignada')
                    ->setCellValue('G6', 'Estatus')
                    ->setCellValue('H6', 'Fecha propuesta');

                $i = 7;

                foreach ($anomalias as $anomalia) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $anomalia->getReferencia())
                        ->setCellValue('B' . $i, date_format($anomalia->getFechaObservacion(), "d-m-Y"))
                        ->setCellValue('C' . $i, $anomalia->getAnomalia())
                        ->setCellValue('D' . $i, $anomalia->getAccionesCorrectivas())
                        ->setCellValue('E' . $i, $anomalia->getSitio()->getNombre())
                        ->setCellValue('F' . $i, $anomalia->getGerencia()->getNombre())
                        ->setCellValue('G' . $i, $anomalia->getEstatus()->getNombre())
                        ->setCellValue('H' . $i, date_format($anomalia->getFechaPropuesta(), "d-m-Y"));
                    $i++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Anomalias');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_tipo_' . $tipo . '_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

        // Reporte por Clasificación (10)
        if( $request->request->get('reporte') == 10 ){
            $desde = $request->get('desde');
            $hasta = $request->get('hasta');
            $clasificacion = $request->get('clasificacion');

            $categoria = "";
            if($clasificacion == "procedimientos") {
                $categoria = "Procedimientos";
            }
            if($clasificacion == "herramientas") {
                $categoria = "Herramientas / Equipos";
            }
            if($clasificacion == "epp") {
                $categoria = "EPP";
            }
            if($clasificacion == "condicion") {
                $categoria = "Condición del Sitio de trabajo";
            }
            if($clasificacion == "sistemas") {
                $categoria = "Sistemas de seguridad";
            }
            if($clasificacion == "comportamiento") {
                $categoria = "Comportamiento";
            }

            $em = $this->getDoctrine()->getManager();

            // Data para el reporte
            $anomalias = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasPorClasificacionByFechas($desde, $hasta, $clasificacion);
            $cantidad = count($anomalias);

            if($cantidad > 0) {
                // Generando el documento Excel
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

                $phpExcelObject->getProperties()->setCreator("YPergas")
                    ->setLastModifiedBy("YPergas")
                    ->setTitle("Sistema de Anomalías")
                    ->setSubject("Sistema de Anomalías")
                    ->setDescription("Sistema de Anomalías. YPergas")
                    ->setKeywords("anomalias");

                $desde = date("d-m-Y", strtotime($desde));
                $hasta = date("d-m-Y", strtotime($hasta));

                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Desde')
                    ->setCellValue('B1', $desde)
                    ->setCellValue('A2', 'Hasta')
                    ->setCellValue('B2', $hasta)
                    ->setCellValue('A3', 'Clasificación')
                    ->setCellValue('B3', $categoria)
                    ->setCellValue('A4', 'Total')
                    ->setCellValue('B4', $cantidad)
                    ->setCellValue('A6', 'Referencia')
                    ->setCellValue('B6', 'Fecha de observación')
                    ->setCellValue('C6', 'Anomalía reportada')
                    ->setCellValue('D6', 'Acciones correctivas')
                    ->setCellValue('E6', 'Sitio')
                    ->setCellValue('F6', 'Gerencia asignada')
                    ->setCellValue('G6', 'Estatus')
                    ->setCellValue('H6', 'Fecha propuesta');

                $i = 7;

                foreach ($anomalias as $anomalia) {
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $anomalia->getReferencia())
                        ->setCellValue('B' . $i, date_format($anomalia->getFechaObservacion(), "d-m-Y"))
                        ->setCellValue('C' . $i, $anomalia->getAnomalia())
                        ->setCellValue('D' . $i, $anomalia->getAccionesCorrectivas())
                        ->setCellValue('E' . $i, $anomalia->getSitio()->getNombre())
                        ->setCellValue('F' . $i, $anomalia->getGerencia()->getNombre())
                        ->setCellValue('G' . $i, $anomalia->getEstatus()->getNombre())
                        ->setCellValue('H' . $i, date_format($anomalia->getFechaPropuesta(), "d-m-Y"));
                    $i++;
                }

                // Nombre de la Hoja
                $phpExcelObject->getActiveSheet()->setTitle('Anomalias');

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $filename = 'ANM_clasificacion_' . $clasificacion . '_' . $generado . '.xlsx';

                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                $dispositionHeader = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
                );

                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', $dispositionHeader);

                return $response;
            }
        }

    }

    /**
     * Pantalla solicitud de parametros
     * Listado por Personas
     *
     * @Route("/3", name="pantalla_porpersonas")
     * @Method("GET")
     */
    public function pantallaReportePorPersonasAction(Request $request)
    {
        return $this->render('reportes/pantalla_por_personas.html.twig', array() );
    }

    /**
     * Listado por Personas
     *
     * @Route("/3/do", name="reporte_porpersonas")
     * @Method("POST")
     */
    public function reportePorPersonasAction(Request $request)
    {
        $idsObservadores = null;
        $totalAnomalias = null;
        $observadores = null;

        $em = $this->getDoctrine()->getManager();
        $desde = $request->get('desde');
        $hasta = $request->get('hasta');

        $anm = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByFechas($desde, $hasta);
        $cantidad = count($anm);

        if($cantidad > 0) {
            $idsObservadores = $em->getRepository('AppBundle:Anomalia')->getIdObservadoresByFechas($desde, $hasta);

            if ($idsObservadores) {
                foreach ($idsObservadores as $idObservador) {
                    // Consulto el totalde anomalias registradas en le fecha (desde,hasta) para el observador
                    $totalAnomalias[] = $em->getRepository('AppBundle:Anomalia')->getTotalAnomaliasByObservadorByFechas($idObservador, $desde, $hasta);
                    $observador = $em->getRepository('AppBundle:Observador')->findOneBy(array('id' => $idObservador));
                    $observadores[] = $observador->getNombre();
                }
            }
        }

        return $this->render('reportes/reporte_por_personas.html.twig', array(
            'desde' => $desde,
            'hasta' => $hasta,
            'observadores' => $observadores,
            'totales' => $totalAnomalias,
            'cantidad' => $cantidad,
        ) );
    }

    /**
     * Pantalla solicitud de parametros
     * Anomalias Pendientes (según vencimiento)
     *
     * @Route("/4", name="pantalla_pendientes")
     * @Method("GET")
     */
    public function pantallaReportePendientesAction(Request $request)
    {
        return $this->render('reportes/pantalla_pendientes.html.twig', array() );
    }

    /**
     * Anomalías Pendientes (según vencimiento)
     *
     * @Route("/4/do", name="reporte_pendientes")
     * @Method("POST")
     */
    public function reportePendientesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $filtro = $request->get('filtro');

        $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByVencimiento($filtro);
        $cantidad = count($anomalias);

        return $this->render('reportes/reporte_pendientes.html.twig', array(
            'filtro' => $filtro,
            'anomalias' => $anomalias,
            'cantidad' => $cantidad,
        ) );
    }

    /**
     * Pantalla solicitud de parametros
     * Anomalias Abiertas Asignadas
     *
     * @Route("/5", name="pantalla_abiertas_asignadas")
     * @Method("GET")
     */
    public function pantallaReporteAbiertasAsignadasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sitios = $em->getRepository('AppBundle:Sitio')->findAllOrderedByName();

        return $this->render('reportes/pantalla_abiertas_asignadas.html.twig', array(
            'sitios' => $sitios,
        ) );
    }

    /**
     * Anomalias Abiertas Asignadas
     *
     * @Route("/5/do", name="reporte_abiertas_asignadas")
     * @Method("POST")
     */
    public function reporteAbiertasAsignadasAction(Request $request)
    {
        $idsGerencias = null;
        $totalAnomalias = null;
        $gerencias = null;

        $em = $this->getDoctrine()->getManager();
        $desde = $request->get('desde');
        $hasta = $request->get('hasta');
        $sitio = $request->get('sitio');

        // Consultando el nombre del sitio
        $s = $em->getRepository('AppBundle:Sitio')->findOneBy(array('id' => $sitio));
        $nombre_sitio = $s->getNombre();

        $anm = $em->getRepository('AppBundle:Anomalia')->getAnomaliasAbiertasSitioByFechas($desde, $hasta, $sitio);
        $cantidad = count($anm);

        if($cantidad > 0) {
            $idsGerencias = $em->getRepository('AppBundle:Anomalia')->getIdGerenciasSitioByFechas($desde, $hasta, $sitio);

            if ($idsGerencias) {
                foreach ($idsGerencias as $idGerencia) {
                    // Consulto el total de anomalias abiertas registradas en le fecha (desde,hasta) y sitio/ubicación para la gerencia
                    $totalAnomalias[] = $em->getRepository('AppBundle:Anomalia')->getTotalAnomaliasAbiertasSitioByGerenciaByFechas($idGerencia, $desde, $hasta, $sitio);
                    $gerencia = $em->getRepository('AppBundle:Gerencia')->findOneBy(array('id' => $idGerencia));
                    $gerencias[] = $gerencia->getNombre();
                }
            }
        }

        return $this->render('reportes/reporte_abiertas_asignadas.html.twig', array(
            'desde' => $desde,
            'hasta' => $hasta,
            'sitio' => $sitio,
            'nombre_sitio' => $nombre_sitio,
            'gerencias' => $gerencias,
            'totales' => $totalAnomalias,
            'cantidad' => $cantidad,
        ) );
    }

    /**
     * Pantalla solicitud de parametros
     * Reporte de anomalias por Regla de Oro
     *
     * @Route("/6", name="pantalla_porregladeoro")
     * @Method("GET")
     */
    public function pantallaReportePorRegladeoroAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reglasdeoro = $em->getRepository('AppBundle:Regladeoro')->findAllOrderedByName();

        return $this->render('reportes/pantalla_por_regladeoro.html.twig', array(
            'reglasdeoro' => $reglasdeoro,
        ) );
    }

    /**
     * Reporte de anomalias por Regla de Oro
     *
     * @Route("/6/do", name="reporte_porregladeoro")
     * @Method("POST")
     */
    public function reportePorRegladeoroAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idRegladeoro = $request->get('regladeoro');

        $regladeoro = $em->getRepository('AppBundle:Regladeoro')->findOneBy(array('id' => $idRegladeoro));
        $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByRegladeoro($regladeoro);
        $cantidad = count($anomalias);

        return $this->render('reportes/reporte_por_regladeoro.html.twig', array(
            'regladeoro' => $regladeoro,
            'anomalias' => $anomalias,
            'idRegladeoro' => $idRegladeoro,
            'cantidad' => $cantidad,
        ) );
    }

    /**
     * Pantalla solicitud de parametros
     * Reporte de anomalias por Gerencia
     *
     * @Route("/7", name="pantalla_porgerencia")
     * @Method("GET")
     */
    public function pantallaReportePorGerenciaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $gerencias = $em->getRepository('AppBundle:Gerencia')->findAllOrderedByName();

        return $this->render('reportes/pantalla_por_gerencia.html.twig', array(
            'gerencias' => $gerencias,
        ) );
    }

    /**
     * Reporte de anomalias por Gerencia
     *
     * @Route("/7/do", name="reporte_porgerencia")
     * @Method("POST")
     */
    public function reportePorGerenciaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idGerencia = $request->get('gerencia');

        $gerencia = $em->getRepository('AppBundle:Gerencia')->findOneBy(array('id' => $idGerencia));
        $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByGerencia($gerencia);
        $cantidad = count($anomalias);

        return $this->render('reportes/reporte_por_gerencia.html.twig', array(
            'gerencia' => $gerencia,
            'anomalias' => $anomalias,
            'idGerencia' => $idGerencia,
            'cantidad' => $cantidad,
        ) );
    }

    /**
     * Pantalla solicitud de parametros
     * Reporte de anomalias por Usuario
     *
     * @Route("/8", name="pantalla_porusuario")
     * @Method("GET")
     */
    public function pantallaReportePorUsuarioAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository('AppBundle:Usuario')->findAllOrderedByUsername();

        return $this->render('reportes/pantalla_por_usuario.html.twig', array(
            'usuarios' => $usuarios,
        ) );
    }

    /**
     * Reporte de anomalias por Usuario
     *
     * @Route("/8/do", name="reporte_porusuario")
     * @Method("POST")
     */
    public function reportePorUsuarioAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idUsuario = $request->get('usuario');

        $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('id' => $idUsuario));
        $anomalias = $em->getRepository('AppBundle:Anomalia')->getAnomaliasByUsuario($usuario);
        $cantidad = count($anomalias);

        return $this->render('reportes/reporte_por_usuario.html.twig', array(
            'usuario' => $usuario,
            'anomalias' => $anomalias,
            'idUsuario' => $idUsuario,
            'cantidad' => $cantidad,
        ) );
    }

    /**
     * Pantalla solicitud de parametros
     * Reporte de anomalias por Tipo de Clasificación
     *
     * @Route("/9", name="pantalla_portipo")
     * @Method("GET")
     */
    public function pantallaReportePorTipoAction(Request $request)
    {
        return $this->render('reportes/pantalla_por_tipo.html.twig', array() );
    }

    /**
     * Anomalias por Tipo de Clasificación
     *
     * @Route("/9/do", name="reporte_portipo")
     * @Method("POST")
     */
    public function reportePorTipoAction(Request $request)
    {
        $totalAnomalias = null;

        $em = $this->getDoctrine()->getManager();
        $desde = $request->get('desde');
        $hasta = $request->get('hasta');
        $tipo = $request->get('tipo');

        $anomalias = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasPorTipoByFechas($desde, $hasta, $tipo);
        $cantidad = count($anomalias);

        return $this->render('reportes/reporte_por_tipo.html.twig', array(
            'desde' => $desde,
            'hasta' => $hasta,
            'tipo' => $tipo,
            'anomalias' => $anomalias,
            'cantidad' => $cantidad,
        ) );
    }

    /**
     * Pantalla solicitud de parametros
     * Reporte de anomalias por Clasificacion
     *
     * @Route("/10", name="pantalla_porclasificacion")
     * @Method("GET")
     */
    public function pantallaReportePorClasificacionAction(Request $request)
    {
        return $this->render('reportes/pantalla_por_clasificacion.html.twig', array() );
    }

    /**
     * Anomalias por Clasificación
     *
     * @Route("/10/do", name="reporte_porclasificacion")
     * @Method("POST")
     */
    public function reportePorClasificacionAction(Request $request)
    {
        $totalAnomalias = null;

        $em = $this->getDoctrine()->getManager();
        $desde = $request->get('desde');
        $hasta = $request->get('hasta');
        $clasificacion = $request->get('clasificacion');

        $anomalias = $em->getRepository('AppBundle:AnomaliaDetalle')->getAnomaliasPorClasificacionByFechas($desde, $hasta, $clasificacion);
        $cantidad = count($anomalias);

        return $this->render('reportes/reporte_por_clasificacion.html.twig', array(
            'desde' => $desde,
            'hasta' => $hasta,
            'clasificacion' => $clasificacion,
            'anomalias' => $anomalias,
            'cantidad' => $cantidad,
        ) );
    }


}
