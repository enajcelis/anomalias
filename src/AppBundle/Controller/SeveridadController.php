<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Severidad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Severidad controller.
 *
 * @Route("severidad")
 */
class SeveridadController extends Controller
{
    /**
     * Lists all severidad entities.
     *
     * @Route("/", name="severidad_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $severidads = $em->getRepository('AppBundle:Severidad')->findAll();

        return $this->render('severidad/index.html.twig', array(
            'severidads' => $severidads,
        ));
    }

    /**
     * Creates a new severidad entity.
     *
     * @Route("/new", name="severidad_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $severidad = new Severidad();
        $form = $this->createForm('AppBundle\Form\SeveridadType', $severidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($severidad);
            $em->flush();

            return $this->redirectToRoute('severidad_show', array('id' => $severidad->getId()));
        }

        return $this->render('severidad/new.html.twig', array(
            'severidad' => $severidad,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a severidad entity.
     *
     * @Route("/{id}", name="severidad_show")
     * @Method("GET")
     */
    public function showAction(Severidad $severidad)
    {
        $deleteForm = $this->createDeleteForm($severidad);

        return $this->render('severidad/show.html.twig', array(
            'severidad' => $severidad,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing severidad entity.
     *
     * @Route("/{id}/edit", name="severidad_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Severidad $severidad)
    {
        $deleteForm = $this->createDeleteForm($severidad);
        $editForm = $this->createForm('AppBundle\Form\SeveridadType', $severidad);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('severidad_edit', array('id' => $severidad->getId()));
        }

        return $this->render('severidad/edit.html.twig', array(
            'severidad' => $severidad,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a severidad entity.
     *
     * @Route("/{id}", name="severidad_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Severidad $severidad)
    {
        $form = $this->createDeleteForm($severidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($severidad);
            $em->flush();
        }

        return $this->redirectToRoute('severidad_index');
    }

    /**
     * Creates a form to delete a severidad entity.
     *
     * @param Severidad $severidad The severidad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Severidad $severidad)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('severidad_delete', array('id' => $severidad->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
