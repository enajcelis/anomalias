<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Sitio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Sitio controller.
 *
 * @Route("sitio")
 */
class SitioController extends Controller
{
    /**
     * Lists all sitio entities.
     *
     * @Route("/", name="sitio_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sitios = $em->getRepository('AppBundle:Sitio')->findAll();

        return $this->render('sitio/index.html.twig', array(
            'sitios' => $sitios,
        ));
    }

    /**
     * Creates a new sitio entity.
     *
     * @Route("/new", name="sitio_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sitio = new Sitio();
        $form = $this->createForm('AppBundle\Form\SitioType', $sitio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sitio);
            $em->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "ADD_SITIO", "ID: ".$sitio->getId().", COD: ".$sitio->getCodigo().", NOMBRE: ".$sitio->getNombre());

            return $this->redirectToRoute('sitio_show', array('id' => $sitio->getId()));
        }

        return $this->render('sitio/new.html.twig', array(
            'sitio' => $sitio,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sitio entity.
     *
     * @Route("/{id}", name="sitio_show")
     * @Method("GET")
     */
    public function showAction(Sitio $sitio)
    {
        $deleteForm = $this->createDeleteForm($sitio);

        return $this->render('sitio/show.html.twig', array(
            'sitio' => $sitio,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing sitio entity.
     *
     * @Route("/{id}/edit", name="sitio_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Sitio $sitio)
    {
        $deleteForm = $this->createDeleteForm($sitio);
        $editForm = $this->createForm('AppBundle\Form\SitioType', $sitio);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "EDIT_SITIO", "ID: ".$sitio->getId().", COD: ".$sitio->getCodigo().", NOMBRE: ".$sitio->getNombre());

            return $this->redirectToRoute('sitio_edit', array('id' => $sitio->getId()));
        }

        return $this->render('sitio/edit.html.twig', array(
            'sitio' => $sitio,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sitio entity.
     *
     * @Route("/{id}", name="sitio_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Sitio $sitio)
    {
        $form = $this->createDeleteForm($sitio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sitio);
            $em->flush();
        }

        return $this->redirectToRoute('sitio_index');
    }

    /**
     * Creates a form to delete a sitio entity.
     *
     * @param Sitio $sitio The sitio entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sitio $sitio)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sitio_delete', array('id' => $sitio->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
