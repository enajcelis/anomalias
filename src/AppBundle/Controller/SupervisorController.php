<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Supervisor;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Supervisor controller.
 *
 * @Route("supervisor")
 */
class SupervisorController extends Controller
{
    /**
     * Lists all supervisor entities.
     *
     * @Route("/", name="supervisor_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $supervisors = $em->getRepository('AppBundle:Supervisor')->findAll();

        return $this->render('supervisor/index.html.twig', array(
            'supervisors' => $supervisors,
        ));
    }

    /**
     * Creates a new supervisor entity.
     *
     * @Route("/new", name="supervisor_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $supervisor = new Supervisor();
        $form = $this->createForm('AppBundle\Form\SupervisorType', $supervisor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($supervisor);
            $em->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "ADD_SUPERVISOR", "ID: ".$supervisor->getId().", NOMBRE: ".$supervisor->getNombre());

            return $this->redirectToRoute('supervisor_show', array('id' => $supervisor->getId()));
        }

        return $this->render('supervisor/new.html.twig', array(
            'supervisor' => $supervisor,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a supervisor entity.
     *
     * @Route("/{id}", name="supervisor_show")
     * @Method("GET")
     */
    public function showAction(Supervisor $supervisor)
    {
        $deleteForm = $this->createDeleteForm($supervisor);

        return $this->render('supervisor/show.html.twig', array(
            'supervisor' => $supervisor,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing supervisor entity.
     *
     * @Route("/{id}/edit", name="supervisor_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Supervisor $supervisor)
    {
        $deleteForm = $this->createDeleteForm($supervisor);
        $editForm = $this->createForm('AppBundle\Form\SupervisorType', $supervisor);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            // Bitacora
            $user = $this->getUser();
            $username = $user->getUsername();
            $bitacora = $this->container->get('BitacoraServices');
            $bitacora->agregarBitacora($username, "EDIT_SUPERVISOR", "ID: ".$supervisor->getId().", NOMBRE: ".$supervisor->getNombre());

            return $this->redirectToRoute('supervisor_edit', array('id' => $supervisor->getId()));
        }

        return $this->render('supervisor/edit.html.twig', array(
            'supervisor' => $supervisor,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a supervisor entity.
     *
     * @Route("/{id}", name="supervisor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Supervisor $supervisor)
    {
        $form = $this->createDeleteForm($supervisor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($supervisor);
            $em->flush();
        }

        return $this->redirectToRoute('supervisor_index');
    }

    /**
     * Creates a form to delete a supervisor entity.
     *
     * @param Supervisor $supervisor The supervisor entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Supervisor $supervisor)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('supervisor_delete', array('id' => $supervisor->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
