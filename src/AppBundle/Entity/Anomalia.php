<?php

namespace AppBundle\Entity;

/**
 * Anomalia
 */
class Anomalia
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $referencia;

    /**
     * @var string
     */
    private $anomalia;

    /**
     * @var string
     */
    private $accionescorrectivas;

    /**
     * @var string
     */
    private $accionespreventivas;

    /**
     * @var \Date
     */
    private $fechaobservacion;

    /**
     * @var \Date
     */
    private $fechapropuesta;

    /**
     * @var \Date
     */
    private $fecharegistro;

    /**
     * @var string
     */
    private $representante;

    /**
     * @var string
     */
    private $otro;

    /**
     * @var integer
     */
    private $activo;

    /**
     * @var \AppBundle\Entity\Sitio
     */
    protected $sitio;

    /**
     * @var \AppBundle\Entity\Observador
     */
    protected $observador;

    /**
     * @var \AppBundle\Entity\Supervisor
     */
    protected $supervisor;

    /**
     * @var \AppBundle\Entity\Gerencia
     */
    protected $gerencia;

    /**
     * @var \AppBundle\Entity\Estatus
     */
    protected $estatus;

    /**
     * @var \AppBundle\Entity\Severidad
     */
    protected $severidad;

    /**
     * @var \AppBundle\Entity\Prioridad
     */
    protected $prioridad;

    /**
     * @var \AppBundle\Entity\Regladeoro
     */
    protected $regladeoro;

    /**
     * @var \AppBundle\Entity\Usuario
     */
    protected $usuario;


    public function __toString()
    {
        return strval( $this->getId() );
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set referencia
     *
     * @return Anomalia
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set anomalia
     *
     * @return Anomalia
     */
    public function setAnomalia($anomalia)
    {
        $this->anomalia = $anomalia;

        return $this;
    }

    /**
     * Get anomalia
     *
     * @return string
     */
    public function getAnomalia()
    {
        return $this->anomalia;
    }

    /**
     * Set accionescorrectivas
     *
     * @return Anomalia
     */
    public function setAccionesCorrectivas($accionescorrectivas)
    {
        $this->accionescorrectivas = $accionescorrectivas;

        return $this;
    }

    /**
     * Get accionescorrectivas
     *
     * @return string
     */
    public function getAccionesCorrectivas()
    {
        return $this->accionescorrectivas;
    }

    /**
     * Set accionespreventivas
     *
     * @return Anomalia
     */
    public function setAccionesPreventivas($accionespreventivas)
    {
        $this->accionespreventivas = $accionespreventivas;

        return $this;
    }

    /**
     * Get accionespreventivas
     *
     * @return string
     */
    public function getAccionesPreventivas()
    {
        return $this->accionespreventivas;
    }

    /**
     * Set fechaobservacion
     *
     * @return Anomalia
     */
    public function setFechaObservacion($fechaobservacion) {
        $this->fechaobservacion = $fechaobservacion;

        return $this;
    }

    /**
     * Get fechaobservacion
     *
     * @return \Date
     */
    public function getFechaObservacion() {
        return $this->fechaobservacion;
    }

    /**
     * Set fechapropuesta
     *
     * @return Anomalia
     */
    public function setFechaPropuesta($fechapropuesta) {
        $this->fechapropuesta = $fechapropuesta;

        return $this;
    }

    /**
     * Get fechapropuesta
     *
     * @return \Date
     */
    public function getFechaPropuesta() {
        return $this->fechapropuesta;
    }

    /**
     * Set fecharegistro
     *
     * @return Anomalia
     */
    public function setFechaRegistro($fecharegistro) {
        $this->fecharegistro = $fecharegistro;

        return $this;
    }

    /**
     * Get fecharegistro
     *
     * @return \DateTime
     */
    public function getFechaRegistro() {
        return $this->fecharegistro;
    }

    /**
     * Get representante
     *
     * @return string
     */
    public function getRepresentante()
    {
        return $this->representante;
    }

    /**
     * Set representante
     *
     * @return Anomalia
     */
    public function setRepresentante($representante)
    {
        $this->representante = $representante;

        return $this;
    }

    /**
     * Get otro
     *
     * @return string
     */
    public function getOtro()
    {
        return $this->otro;
    }

    /**
     * Set otro
     *
     * @return Anomalia
     */
    public function setOtro($otro)
    {
        $this->otro = $otro;

        return $this;
    }

    /**
     * Get activo
     *
     * @return integer
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set activo
     *
     * @return Anomalia
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get sitio
     *
     * @return \AppBundle\Entity\Sitio
     */
    public function getSitio()
    {
        return $this->sitio;
    }

    /**
     * Set sitio
     *
     * @return Anomalia
     */
    public function setSitio($sitio)
    {
        $this->sitio = $sitio;

        return $this;
    }

    /**
     * Get observador
     *
     * @return \AppBundle\Entity\Observador
     */
    public function getObservador()
    {
        return $this->observador;
    }

    /**
     * Set observador
     *
     * @return Anomalia
     */
    public function setObservador($observador)
    {
        $this->observador = $observador;

        return $this;
    }

    /**
     * Get supervisor
     *
     * @return \AppBundle\Entity\Supervisor
     */
    public function getSupervisor()
    {
        return $this->supervisor;
    }

    /**
     * Set supervisor
     *
     * @return Anomalia
     */
    public function setSupervisor($supervisor)
    {
        $this->supervisor = $supervisor;

        return $this;
    }

    /**
     * Get gerencia
     *
     * @return \AppBundle\Entity\Gerencia
     */
    public function getGerencia()
    {
        return $this->gerencia;
    }

    /**
     * Set gerencia
     *
     * @return Anomalia
     */
    public function setGerencia($gerencia)
    {
        $this->gerencia = $gerencia;

        return $this;
    }

    /**
     * Get estatus
     *
     * @return \AppBundle\Entity\Estatus
     */
    public function getEstatus()
    {
        return $this->estatus;
    }

    /**
     * Set estatus
     *
     * @return Anomalia
     */
    public function setEstatus($estatus)
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * Get severidad
     *
     * @return \AppBundle\Entity\Severidad
     */
    public function getSeveridad()
    {
        return $this->severidad;
    }

    /**
     * Set severidad
     *
     * @return Anomalia
     */
    public function setSeveridad($severidad)
    {
        $this->severidad = $severidad;

        return $this;
    }

    /**
     * Get prioridad
     *
     * @return \AppBundle\Entity\Prioridad
     */
    public function getPrioridad()
    {
        return $this->prioridad;
    }

    /**
     * Set prioridad
     *
     * @return Anomalia
     */
    public function setPrioridad($prioridad)
    {
        $this->prioridad = $prioridad;

        return $this;
    }

    /**
     * Get regladeoro
     *
     * @return \AppBundle\Entity\Regladeoro
     */
    public function getRegladeoro()
    {
        return $this->regladeoro;
    }

    /**
     * Set regladeoro
     *
     * @return Anomalia
     */
    public function setRegladeoro($regladeoro)
    {
        $this->regladeoro = $regladeoro;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set usuario
     *
     * @return Anomalia
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

}
