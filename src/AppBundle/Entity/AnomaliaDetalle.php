<?php

namespace AppBundle\Entity;

/**
 * AnomaliaDetalle
 */
class AnomaliaDetalle
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $anomalia;

    /**
     * @var string
     */
    private $clasificacion;


    public function __toString()
    {
        return strval( $this->getId() );
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set anomalia
     *
     * @param \AppBundle\Entity\Anomalia $anomalia
     * @return AnomaliaDetalle
     */
    public function setAnomalia(\AppBundle\Entity\Anomalia $anomalia = null)
    {
        $this->anomalia = $anomalia;

        return $this;
    }

    /**
     * Get anomalia
     *
     * @return \AppBundle\Entity\Anomalia
     */
    public function getAnomalia()
    {
        return $this->anomalia;
    }

    /**
     * Set clasificacion
     *
     * @param \AppBundle\Entity\Clasificacion $clasificacion
     * @return AnomaliaDetalle
     */
    public function setClasificacion(\AppBundle\Entity\Clasificacion $clasificacion = null)
    {
        $this->clasificacion = $clasificacion;

        return $this;
    }

    /**
     * Get clasificacion
     *
     * @return \AppBundle\Entity\Clasificacion
     */
    public function getClasificacion()
    {
        return $this->clasificacion;
    }
}
