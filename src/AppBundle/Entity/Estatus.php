<?php

namespace AppBundle\Entity;

/**
 * Estatus
 */
class Estatus
{

    const ID_ABIERTA = 1;
    const ID_CERRADA = 2;

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $nombre;


    public function __toString()
    {
        return strval( $this->getId() );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }    
    
    /**
     * Set nombre
     *
     * @return Estatus
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}
