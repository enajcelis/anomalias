<?php

namespace AppBundle\Entity;

/**
 * Observador
 */
class Observador
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $nombre;
    
    /**
     * @var string
     */
    private $correo;

    /**
     * @var \AppBundle\Entity\Gerencia
     */
    protected $gerencia;


    public function __toString()
    {
        return strval( $this->getId() );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }    
    
    /**
     * Set nombre
     *
     * @return Observador
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * Set correo
     *
     * @return Observador
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Get gerencia
     *
     * @return \AppBundle\Entity\Gerencia
     */
    public function getGerencia()
    {
        return $this->gerencia;
    }

    /**
     * Set gerencia
     *
     * @return Anomalia
     */
    public function setGerencia($gerencia)
    {
        $this->gerencia = $gerencia;

        return $this;
    }

}
