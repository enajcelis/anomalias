<?php

namespace AppBundle\Entity;

/**
 * Prioridad
 */
class Prioridad
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $valor;


    public function __toString()
    {
        return strval( $this->getId() );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }    
    
    /**
     * Set valor
     *
     * @return Prioridad
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }
}
