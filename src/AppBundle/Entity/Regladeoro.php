<?php

namespace AppBundle\Entity;

/**
 * Regladeoro
 */
class Regladeoro
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $codigo;
    
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $icono;



    public function __toString()
    {
        return strval( $this->getId() );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }    
    
    /**
     * Set codigo
     *
     * @return Regladeoro
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    
    /**
     * Set descripcion
     *
     * @return Regladeoro
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set icono
     *
     * @return Clasificacion
     */
    public function setIcono($icono)
    {
        $this->icono = $icono;

        return $this;
    }

    /**
     * Get icono
     *
     * @return string
     */
    public function getIcono()
    {
        return $this->icono;
    }
}
