<?php

namespace AppBundle\Entity;

/**
 * Severidad
 */
class Severidad
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    private $valor;


    public function __toString()
    {
        return strval( $this->getId() );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }    
    
    /**
     * Set valor
     *
     * @return Severidad
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return integer 
     */
    public function getValor()
    {
        return $this->valor;
    }
}
