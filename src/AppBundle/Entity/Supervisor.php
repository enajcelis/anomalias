<?php

namespace AppBundle\Entity;

/**
 * Supervisor
 */
class Supervisor
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $nombre;
    
    /**
     * @var string
     */
    private $correo;


    public function __toString()
    {
        return strval( $this->getId() );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }    
    
    /**
     * Set nombre
     *
     * @return Supervisor
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * Set correo
     *
     * @return Supervisor
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }

}
