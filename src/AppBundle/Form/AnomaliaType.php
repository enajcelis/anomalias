<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class AnomaliaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('referencia', TextType::class,['attr' => array('class' => 'input-grande')])
            ->add('anomalia', TextareaType::class)
            ->add('accionescorrectivas', TextareaType::class)
            ->add('accionespreventivas', TextareaType::class)
            ->add('fechaobservacion', DateType::class,
                [
                    'widget' => 'single_text',
                    'required'=>true,
                    'html5' => false,
                    'attr' =>
                        [
                            'class'=>'datepicker',
                            'data-date-format'=>'YYYY-MM-DD'
                        ]
                ]
            )
            ->add('fechapropuesta', DateType::class,
                [
                    'widget' => 'single_text',
                    'required'=>true,
                    'html5' => false,
                    'attr' =>
                        [
                            'class'=>'datepicker',
                            'data-date-format'=>'YYYY-MM-DD'
                        ]
                ]
            )
            ->add('representante', TextType::class,[
                'attr' => array('class' => 'input-grande')])
            ->add('otro', TextType::class,
                [
                'required'=>false,
                ]
            )
            ->add('sitio', EntityType::class, [
                'class' => 'AppBundle\Entity\Sitio',
                'choice_label' => 'nombre',
                'placeholder' => ' -- Seleccione-- ',
                'attr' => array('class' => 'input-grande')
            ])
            ->add('observador', EntityType::class, [
                'class' => 'AppBundle\Entity\Observador',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->orderBy('o.nombre', 'ASC');
                },
                'choice_label' => 'nombre',
                'placeholder' => ' -- Seleccione una opción -- ',
                'attr' => array('class' => 'input-grande')
            ])
            ->add('supervisor', EntityType::class, [
                'class' => 'AppBundle\Entity\Supervisor',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.nombre', 'ASC');
                },
                'choice_label' => 'nombre',
                'placeholder' => ' -- Seleccione una opción -- ',
                'attr' => array('class' => 'input-grande')
            ])
            ->add('gerencia', EntityType::class, [
                'class' => 'AppBundle\Entity\Gerencia',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.nombre', 'ASC');
                },
                'choice_label' => 'nombre',
                'placeholder' => ' -- Seleccione una opción -- ',
                'attr' => array('class' => 'input-grande')
            ])
            ->add('estatus', EntityType::class, [
                'class' => 'AppBundle\Entity\Estatus',
                'choice_label' => 'nombre',
                'placeholder' => ' -- Seleccione una opción -- ',
                'attr' => array('class' => 'input-grande')
            ])
            ->add('severidad', EntityType::class, [
                'class' => 'AppBundle\Entity\Severidad',
                'choice_label' => 'valor',
                'placeholder' => ' -- Seleccione -- ',
                'attr' => array('class' => 'input-grande')
            ])
            ->add('prioridad', EntityType::class, [
                'class' => 'AppBundle\Entity\Prioridad',
                'choice_label' => 'valor',
                'placeholder' => ' -- Seleccione -- ',
                'attr' => array('class' => 'input-grande')
            ])
            ->add('regladeoro', EntityType::class, [
                'class' => 'AppBundle\Entity\Regladeoro',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                        ->orderBy('r.descripcion', 'ASC');
                },
                'choice_label' => 'descripcion',
                'placeholder' => ' -- Seleccione una opción -- ',
                'attr' => array('class' => 'input-grande')
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Anomalia'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_anomalia';
    }


}
