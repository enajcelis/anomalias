<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ClasificacionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('codigo', TextType::class,
                [
                    'label'=>'Código',
                    'required'=>true
                ]
            )
            ->add('descripcion', TextType::class,
                [
                    'label'=>'Descripción',
                    'required'=>true
                ]
            )
            ->add('categoria', ChoiceType::class,
                [
                    'choices' => [
                        'Procedimientos' => 'procedimientos',
                        'Herramientas / Equipos' => 'herramientas',
                        'EPP' => 'epp',
                        'Condición del Sitio de trabajo' => 'condicion',
                        'Sistemas de seguridad' => 'sistemas',
                        'Comportamiento' => 'comportamiento',
                    ],
                    'placeholder' => ' -- Seleccione una opción -- ',
                    'label' => 'Categoría',
                    'required'=>true
                ]
            )
            ->add('tipo', ChoiceType::class,
                [
                    'choices' => [
                        'Acto inseguro' => 'acto',
                        'Condición insegura' => 'condicion',
                    ],
                    'placeholder' => ' -- Seleccione una opción -- ',
                    'label' => 'Tipo',
                    'required'=>true
                ]
            );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Clasificacion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_clasificacion';
    }


}
