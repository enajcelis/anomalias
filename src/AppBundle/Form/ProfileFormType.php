<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre', TextType::class, ['label' => 'profile.show.name', 'translation_domain' => 'FOSUserBundle', 'required' => true])
                ->add('apellido', TextType::class, ['label' => 'profile.show.lastname', 'translation_domain' => 'FOSUserBundle', 'required' => true])
                ->add('imageFile', FileType::class, array('label' => 'profile.show.picture', 'translation_domain' => 'FOSUserBundle', 'data_class' => null, 'required' => false));
    }

public function getParent() 
{
    return 'FOS\UserBundle\Form\Type\ProfileFormType';
}

public function getBlockPrefix()
{
    return 'app_user_profile';
}

// For Symfony 2.x
public function getName()
{
    return $this->getBlockPrefix();
}

}