<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('nombre', TextType::class,
            [
                'attr' => array('class' => 'input-grande'),
                'required' => true,
                'label' => 'Nombre'
            ])
            ->add('apellido', TextType::class,
            [
                'attr' => array('class' => 'input-grande'),
                'required' => true,
                'label' => 'Apellido'
            ])
        ;
    }
    public function getParent() {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix() {
        return 'app_user_registration';
    }

    public function getName() {
        return $this->getBlockPrefix();
    }
}