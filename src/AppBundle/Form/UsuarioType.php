<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class UsuarioType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('nombre', TextType::class,
            [
                'attr' => array('class' => 'input-grande'),
                'required' => true,
                'label' => 'Nombre'
            ])
            ->add('apellido', TextType::class,
            [
                'attr' => array('class' => 'input-grande'),
                'required' => true,
                'label' => 'Apellido'
            ])
            ->add('email', EmailType::class,
            [
                'attr' => array('class' => 'input-grande'),
                'required' => true,
                'label' => 'Correo electrónico'
            ])
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Usuario'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_usuario';
    }
}