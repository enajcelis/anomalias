<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AnomaliaDetalleRepository extends EntityRepository{

    /**
     * Detalles de la Anomalia según la categoria y anomalia especificada
     */
    public function getAnomaliasDetallesByCategoria($anomalia, $categoria) {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c '
                . 'FROM AppBundle:AnomaliaDetalle a, AppBundle:Clasificacion c '
                . 'WHERE a.clasificacion = c.id '
                . 'AND c.categoria = ?1 '
                . 'AND a.anomalia = ?2 '
                . 'ORDER BY c.descripcion'
            )
            ->setParameter(1, $categoria)
            ->setParameter(2, $anomalia)
            ->getResult();
    }

    /**
     * Codigos de Clasificaciones asociadas a la anomalia especificada
     */
    public function getClasificacionesInAnomaliaDetalle($anomalia) {
        $idClasificaciones = null;

        $detalles = $this->getEntityManager()
            ->createQuery(
                'SELECT c '
                . 'FROM AppBundle:AnomaliaDetalle a, AppBundle:Clasificacion c '
                . 'WHERE a.clasificacion = c.id '
                . 'AND a.anomalia = ?1 '
            )
            ->setParameter(1, $anomalia)
            ->getResult();

        foreach ($detalles as $detalle) {
            $idClasificaciones[] = $detalle->getCodigo();
        }

        return $idClasificaciones;
    }

    /**
     * Anomalias según el tipo de clasificación (acto, condicion) para el periodo de fechas especificado
     */
    public function getAnomaliasPorTipoByFechas($desde, $hasta, $tipo) {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT a '
                . 'FROM AppBundle:AnomaliaDetalle ad, AppBundle:Anomalia a, AppBundle:Clasificacion c '
                . 'WHERE a.id = ad.anomalia AND ad.clasificacion = c.id '
                . 'AND a.fechaobservacion >= :desde AND a.fechaobservacion <= :hasta '
                . 'AND c.tipo = :tipo '
                . 'AND a.activo = 1 '
            )
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->setParameter('tipo', $tipo)
            ->getResult();
    }

    /**
     * Anomalias según la clasificación (procedimientos, herramientas, epp, condicion, sistemas, comportamiento) para el periodo de fechas especificado
     */
    public function getAnomaliasPorClasificacionByFechas($desde, $hasta, $clasificacion) {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT a '
                . 'FROM AppBundle:AnomaliaDetalle ad, AppBundle:Anomalia a, AppBundle:Clasificacion c '
                . 'WHERE a.id = ad.anomalia AND ad.clasificacion = c.id '
                . 'AND a.fechaobservacion >= :desde AND a.fechaobservacion <= :hasta '
                . 'AND c.categoria = :clasificacion '
                . 'AND a.activo = 1 '
            )
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->setParameter('clasificacion', $clasificacion)
            ->getResult();
    }

}
