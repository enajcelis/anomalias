<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AnomaliaRepository extends EntityRepository{

    /**
     * Anomalias segun el valor del campo 'activo' especificado
     */
    public function findByActivo($activo)
    {
        $query = $this->createQueryBuilder('a')
            ->Select('a')
            ->where('a.activo = :activo ' )
            ->setParameter('activo', $activo)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Total de anomalias registradas
     */
    public function getTotalAnomalias() {
        $query = $this->createQueryBuilder('a')
            ->Select('COUNT(a.id) as total')
            ->where('a.activo = 1' )
            ->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * Total de anomalias segun el estatus especificado
     */
    public function getTotalAnomaliasByEstatus($estatus) {
        $query = $this->createQueryBuilder('a')
            ->Select('COUNT(a.id) as total')
            ->join('a.estatus', 'e')
            ->where('e.id = :estatus AND a.activo = 1' )
            ->setParameter('estatus', $estatus)
            ->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * Anomalias registradas en la fecha especificada
     */
    public function getTotalAnomaliasByFecha($fecha) {

        if($fecha == "now"){
            return $this->getEntityManager()
                ->createQuery(
                    'SELECT COUNT(a.id) as total '
                    . 'FROM AppBundle:Anomalia a '
                    . 'WHERE TO_CHAR(a.fecharegistro, \'YY/MM/DD\') = TO_CHAR(CURRENT_DATE(),\'YY/MM/DD\') '
                    . 'AND a.activo = 1'
                )
                ->getSingleScalarResult();
        }

    }

    /**
     * Anomalias registradas para el observador especificado
     */
    public function getAnomaliasByObservador($observador) {
        $query = $this->createQueryBuilder('a')
            ->Select('a')
            ->join('a.observador', 'o')
            ->where('o.id = :observador AND a.activo = 1' )
            ->setParameter('observador', $observador)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Anomalias registradas para el sitio / ubicación especificado
     */
    public function getAnomaliasBySitio($sitio) {
        $query = $this->createQueryBuilder('a')
            ->Select('a')
            ->join('a.sitio', 's')
            ->where('s.id = :sitio AND a.activo = 1' )
            ->setParameter('sitio', $sitio)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Anomalias para el periodo de fechas especificado
     */
    public function getAnomaliasByFechas($desde, $hasta) {
        $query = $this->createQueryBuilder('a')
            ->Select('a')
            ->where('a.fechaobservacion >= :desde AND a.fechaobservacion <= :hasta AND a.activo = 1 ' )
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Anomalias para el periodo de fechas especificado, agrupadas por observador
     */
    public function getIdObservadoresByFechas($desde, $hasta) {

        $idsObservadores = null;

        $query = $this->createQueryBuilder('a')
            ->Select('IDENTITY(a.observador)')
            ->where('a.fechaobservacion >= :desde AND a.fechaobservacion <= :hasta AND a.activo = 1 ' )
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->groupBy('a.observador')
            ->getQuery();

        $observadores = $query->getResult();

        foreach ($observadores as $observador) {
            $idsObservadores[] = $observador[1];
        }

        return $idsObservadores;
    }

    /**
     * Total de Anomalias para el observador en el periodo de fechas especificado
     */
    public function getTotalAnomaliasByObservadorByFechas($idObservador, $desde, $hasta) {
        $query = $this->createQueryBuilder('a')
            ->Select('COUNT(a.id) as total')
            ->where('a.fechaobservacion >= :desde AND a.fechaobservacion <= :hasta AND a.observador = :observador AND a.activo = 1 ' )
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->setParameter('observador', $idObservador)
            ->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * Anomalias pendientes segun vencimiento
     * Estatus de anomalias = Abierta (1)
     */
    public function getAnomaliasByVencimiento($filtro) {
        $fecha = new \DateTime('now');

        if($filtro == "vencidas"){
            $query = $this->createQueryBuilder('a')
                ->Select('a')
                ->where('a.fechapropuesta < :fecha AND a.estatus = 1 AND a.activo = 1' )
                ->setParameter('fecha', $fecha)
                ->getQuery();
        }elseif ($filtro == "novencidas"){
            $query = $this->createQueryBuilder('a')
                ->Select('a')
                ->where('a.fechapropuesta >= :fecha AND a.estatus =1 AND a.activo = 1' )
                ->setParameter('fecha', $fecha)
                ->getQuery();
        }

        return $query->getResult();
    }

    /**
     * Anomalias Abiertas Asigandas para el periodo de fechas y sitio/ubicación especificado
     */
    public function getAnomaliasAbiertasAsignadas($desde, $hasta, $sitio) {
        $query = $this->createQueryBuilder('a')
            ->Select('a')
            ->where('a.fechaobservacion >= :desde AND a.fechaobservacion <= :hasta AND a.sitio = :sitio AND a.estatus = 1 AND (a.gerencia IS NOT NULL OR a.supervisor IS NOT NULL)' )
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->setParameter('sitio', $sitio)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Anomalias Abiertas (1) para el periodo de fechas y sitio/ubicación especificado
     */
    public function getAnomaliasAbiertasSitioByFechas($desde, $hasta, $sitio) {
        $query = $this->createQueryBuilder('a')
            ->Select('a')
            ->where('a.fechaobservacion >= :desde AND a.fechaobservacion <= :hasta AND a.sitio = :sitio AND a.estatus = 1 AND a.activo = 1 ' )
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->setParameter('sitio', $sitio)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Anomalias Abiertas (1) para el periodo de fechasy sitio/ubicación especificado, agrupadas por gerencia
     */
    public function getIdGerenciasSitioByFechas($desde, $hasta, $sitio) {

        $idsGerencias = null;

        $query = $this->createQueryBuilder('a')
            ->Select('IDENTITY(a.gerencia)')
            ->where('a.fechaobservacion >= :desde AND a.fechaobservacion <= :hasta AND a.sitio = :sitio AND a.estatus = 1 AND a.activo = 1 ' )
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->setParameter('sitio', $sitio)
            ->groupBy('a.gerencia')
            ->getQuery();

        $gerencias = $query->getResult();

        foreach ($gerencias as $gerencia) {
            $idsGerencias[] = $gerencia[1];
        }

        return $idsGerencias;
    }

    /**
     * Total de Anomalias Abiertas para la gerencia en el periodo de fechas y sitio especificado
     */
    public function getTotalAnomaliasAbiertasSitioByGerenciaByFechas($idGerencia, $desde, $hasta, $sitio) {
        $query = $this->createQueryBuilder('a')
            ->Select('COUNT(a.id) as total')
            ->where('a.fechaobservacion >= :desde AND a.fechaobservacion <= :hasta AND a.gerencia = :gerencia AND a.sitio = :sitio AND a.estatus = 1 AND a.activo = 1 ' )
            ->setParameter('desde', $desde)
            ->setParameter('hasta', $hasta)
            ->setParameter('sitio', $sitio)
            ->setParameter('gerencia', $idGerencia)
            ->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * Anomalias registradas para la Regla de Oro especificada
     */
    public function getAnomaliasByRegladeoro($regladeoro) {
        $query = $this->createQueryBuilder('a')
            ->Select('a')
            ->join('a.regladeoro', 'r')
            ->where('r.id = :regla AND a.activo = 1' )
            ->setParameter('regla', $regladeoro)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Anomalias registradas para Observadores de la Gerencia especificada
     */
    public function getAnomaliasByGerencia($gerencia) {
        $query = $this->createQueryBuilder('a')
            ->Select('a')
            ->join('a.observador', 'o')
            ->where('o.gerencia = :gerencia AND a.activo = 1' )
            ->setParameter('gerencia', $gerencia)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Ultimo ID de Anomalía
     */
    public function getLastIDAnomalia() {
        $query = $this->createQueryBuilder('a')
            ->Select('MAX(a.id)')
            ->getQuery();

        return $query->getSingleScalarResult();
    }

    /**
     * Anomalias registradas por el usuario especificado
     */
    public function getAnomaliasByUsuario($usuario) {
        $query = $this->createQueryBuilder('a')
            ->Select('a')
            ->join('a.usuario', 'o')
            ->where('o.id = :usuario AND a.activo = 1' )
            ->setParameter('usuario', $usuario)
            ->getQuery();

        return $query->getResult();
    }

}
