<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ClasificacionRepository extends EntityRepository{

    /**
     * Clasificaciones según la categoria especificada
     */
    public function getAllClasificacionesByCategoria($categoria) {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c '
                . 'FROM AppBundle:Clasificacion c '
                . 'WHERE c.categoria = ?1 '
                . 'ORDER BY c.descripcion'
            )
            ->setParameter(1, $categoria)
            ->getResult();
    }

}
