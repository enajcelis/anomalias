<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GerenciaRepository extends EntityRepository{

    /**
     * Gerencias ordenadas por nombre ASC
     */
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT g FROM AppBundle:Gerencia g ORDER BY g.nombre ASC'
            )
            ->getResult();
    }
}
