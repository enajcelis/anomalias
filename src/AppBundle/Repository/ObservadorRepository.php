<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ObservadorRepository extends EntityRepository{

    /**
     * Observadores ordenados por nombre ASC
     */
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT o FROM AppBundle:Observador o ORDER BY o.nombre ASC'
            )
            ->getResult();
    }
}
