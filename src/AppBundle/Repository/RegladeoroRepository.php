<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class RegladeoroRepository extends EntityRepository{

    /**
     * Reglas de Oro ordenadas por nombre ASC
     */
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT r FROM AppBundle:Regladeoro r ORDER BY r.descripcion ASC'
            )
            ->getResult();
    }
}
