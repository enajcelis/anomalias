<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SitioRepository extends EntityRepository{

    /**
     * Sitios/Ubicaciones ordenadas por nombre ASC
     */
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT s FROM AppBundle:Sitio s ORDER BY s.nombre ASC'
            )
            ->getResult();
    }
}
