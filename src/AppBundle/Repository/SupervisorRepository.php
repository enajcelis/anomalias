<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SupervisorRepository extends EntityRepository{

    /**
     * Supervisores ordenados por nombre ASC
     */
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT s FROM AppBundle:Supervisor s ORDER BY s.nombre ASC'
            )
            ->getResult();
    }
}
