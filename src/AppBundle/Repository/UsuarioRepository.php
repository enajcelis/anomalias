<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UsuarioRepository extends EntityRepository{

    /**
     * Usuarios ordenados por username ASC
     */
    public function findAllOrderedByUsername()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT o FROM AppBundle:Usuario o ORDER BY o.nombre ASC'
            )
            ->getResult();
    }
}
