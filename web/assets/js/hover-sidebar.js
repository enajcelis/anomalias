$(document).ready(function(){
    $("#sede_menu").hover(function(){
            $("#sede_icon").css({'color':'darkviolet'});
        }, function() {
            $("#sede_icon").css({'color':''});
        }
    );

    $("#gestion_casos_menu").hover(function(){
            $("#gestion_casos_icon").css({'color':'cornflowerblue'});
        }, function() {
            $("#gestion_casos_icon").css({'color':''});
        }
    );

    $("#clientes_menu").hover(function(){
            $("#clientes_icon").css({'color':'antiquewhite'});
        }, function() {
            $("#clientes_icon").css({'color':''});
        }
    );

    $("#asociacion_menu").hover(function(){
            $("#asociacion_icon").css({'color':'coral'});
        }, function() {
            $("#asociacion_icon").css({'color':''});
        }
    );

    $("#casos_menu").hover(function(){
            $("#casos_icon").css({'color':'gold'});
        }, function() {
            $("#casos_icon").css({'color':''});
        }
    );

    $("#provision_menu").hover(function(){
            $("#provision_icon").css({'color':'darkseagreen'});
        }, function() {
            $("#provision_icon").css({'color':''});
        }
    );

    $("#facturacion_menu").hover(function(){
            $("#facturacion_icon").css({'color':'cornsilk'});
        }, function() {
            $("#facturacion_icon").css({'color':''});
        }
    );

    $("#avances_menu").hover(function(){
            $("#avances_icon").css({'color':'yellow'});
        }, function() {
            $("#avances_icon").css({'color':''});
        }
    );

    $("#suplidos_menu").hover(function(){
            $("#suplidos_icon").css({'color':'tomato'});
        }, function() {
            $("#suplidos_icon").css({'color':''});
        }
    );

    $("#partes_menu").hover(function(){
            $("#partes_icon").css({'color':'lawngreen'});
        }, function() {
            $("#partes_icon").css({'color':''});
        }
    );

    $("#eventos_menu").hover(function(){
            $("#eventos_icon").css({'color':'fuchsia'});
        }, function() {
            $("#eventos_icon").css({'color':''});
        }
    );

    $("#boletines_menu").hover(function(){
            $("#boletines_icon").css({'color':'seashell'});
        }, function() {
            $("#boletines_icon").css({'color':''});
        }
    );
});